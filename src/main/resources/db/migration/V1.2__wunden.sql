CREATE TYPE wunden_location AS ENUM (
    'kopf', 'brust', 'arm_links', 'arm_rechts', 'bauch', 'bein_links', 'bein_rechts'
    );

CREATE TABLE "wunde"
(
    "id"           BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    "character_id" BIGINT          NOT NULL,
    "location"     wunden_location NOT NULL,
    "received_at"  TIMESTAMP       NOT NULL DEFAULT current_timestamp
);

ALTER TABLE "wunde"
    ADD CONSTRAINT "FK_wunde_character"
        FOREIGN KEY ("character_id") REFERENCES "character" ("id");
