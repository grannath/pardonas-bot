package de.grannath.pardona.characters

import de.grannath.pardona.jooq.enums.WundenLocation
import java.util.Locale

typealias User = de.grannath.pardona.jooq.tables.pojos.User

typealias Character = de.grannath.pardona.jooq.tables.pojos.Character

typealias CharacterStatus = de.grannath.pardona.jooq.tables.pojos.CharacterStatus

typealias Wunde = de.grannath.pardona.jooq.tables.pojos.Wunde

fun WundenLocation.asString() =
    toString().replace('_', ' ')
        .replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.GERMAN) else it.toString() }

data class CharacterEigenschaft(
    val name: String,
    val value: Int,
) {
    val shortForm = name.shortenEigenschaft()
}

data class CharacterTalent(
    override val name: String,
    override val value: Int,
    override val check: Triple<String, String, String>,
) : CheckValue()

data class CharacterZauber(
    override val name: String,
    override val value: Int,
    override val check: Triple<String, String, String>,
) : CheckValue()

sealed class CheckValue {
    abstract val name: String

    abstract val value: Int

    abstract val check: Triple<String, String, String>
}

private val eigenschaftShortForms = mapOf(
    "mu" to "mut",
    "kl" to "klugheit",
    "in" to "intuition",
    "ch" to "charisma",
    "ff" to "fingerfertigkeit",
    "ge" to "gewandtheit",
    "ko" to "konstitution",
    "kk" to "körperkraft",
    "at" to "attacke",
    "pa" to "parade",
    "ini" to "initiative",
    "gs" to "geschwindigkeit"
)

fun String.normalize() =
    lowercase(Locale.GERMAN)
        .replace("ü", "ue")
        .replace("ö", "oe")
        .replace("ä", "ae")
        .replace("ß", "ss")
        .replace("heilkunde", "hk")
        .replace(":", "")
        .replace("sprachen kennen", "")
        .replace("lesen/schreiben", "")
        .replace("\\s+".toRegex(), "")

fun String.expandEigenschaft(): String? =
    eigenschaftShortForms.getOrDefault(this.lowercase(Locale.GERMAN), null)

private fun String.shortenEigenschaft(): String =
    eigenschaftShortForms.keys.singleOrNull { it == this.lowercase(Locale.GERMAN) }?.uppercase(Locale.GERMAN)
        ?: eigenschaftShortForms.entries.singleOrNull { (_, value) -> value.startsWith(this.lowercase(Locale.GERMAN)) }
            ?.key
            ?.uppercase(Locale.GERMAN)
        ?: this

sealed class Modifier {
    abstract val name: String
    abstract val modifier: Int

    open fun asString() = "$name: $modifier"
}

data class EigenschaftModifier(val eigenschaft: String, override val modifier: Int) : Modifier() {
    init {
        require(eigenschaftShortForms.containsKey(eigenschaft)) { "Unknown Eigenschaft $eigenschaft" }
    }

    override val name: String
        get() = eigenschaft.uppercase(Locale.GERMAN)
}

data class AllEigenschaftenModifier(override val modifier: Int) : Modifier() {
    override val name = "Alle Eigenschaften"
}

data class TalentModifier(val talent: String, override val modifier: Int) : Modifier() {
    override val name = talent
}

data class TalentGroupModifier(val groupName: String, val talente: List<String>, override val modifier: Int) :
    Modifier() {
    override val name = groupName
}

data class AllTalenteModifier(override val modifier: Int) : Modifier() {
    override val name = "Alle Talente"
}

data class AllZauberModifier(override val modifier: Int) : Modifier() {
    override val name = "Alle Zauber"
}

data class NamedModifier(override val name: String) : Modifier() {
    override val modifier = 0
    override fun asString() = name
}

data class State(
    val name: String,
    val modifiers: List<Modifier>
) {
    fun asString(): String =
        "$name (${modifiers.joinToString(separator = ", ") { it.asString() }})"
}

fun State(name: String) = State(name, listOf())

fun State.and(name: String, modifier: Int) =
    if (eigenschaftShortForms.containsKey(name)) {
        this.copy(modifiers = this.modifiers.merge(EigenschaftModifier(name, modifier)))
    } else {
        this.copy(modifiers = this.modifiers.merge(TalentModifier(name, modifier)))
    }

fun State.and(name: String) =
    this.copy(modifiers = this.modifiers.merge(NamedModifier(name)))

fun State.andEigenschaften(modifier: Int) =
    this.copy(modifiers = this.modifiers.merge(AllEigenschaftenModifier(modifier)))

fun State.andTalente(modifier: Int) =
    this.copy(modifiers = this.modifiers.merge(AllTalenteModifier(modifier)))

fun State.andZauber(modifier: Int) =
    this.copy(modifiers = this.modifiers.merge(AllZauberModifier(modifier)))

private fun List<Modifier>.merge(other: Modifier): List<Modifier> =
    when (other) {
        is AllEigenschaftenModifier ->
            replaceOrAdd(other) { if (it is AllEigenschaftenModifier) it.copy(modifier = it.modifier + other.modifier) else null }
        is AllTalenteModifier ->
            replaceOrAdd(other) { if (it is AllTalenteModifier) it.copy(modifier = it.modifier + other.modifier) else null }
        is AllZauberModifier ->
            replaceOrAdd(other) { if (it is AllZauberModifier) it.copy(modifier = it.modifier + other.modifier) else null }
        is EigenschaftModifier ->
            replaceOrAdd(other) { if (it is EigenschaftModifier && it.name == other.name) it.copy(modifier = it.modifier + other.modifier) else null }
        is TalentGroupModifier ->
            replaceOrAdd(other) { if (it is TalentGroupModifier && it.name == other.name) it.copy(modifier = it.modifier + other.modifier) else null }
        is TalentModifier ->
            replaceOrAdd(other) { if (it is TalentModifier && it.name == other.name) it.copy(modifier = it.modifier + other.modifier) else null }
        is NamedModifier ->
            this + other
    }

private fun <T : Any> List<T>.replaceOrAdd(new: T, map: (T) -> T?): List<T> {
    for (it in this) {
        val replacement = map(it)
        if (replacement != null) {
            return this - it + replacement
        }
    }
    return this + new
}

private fun List<Modifier>.merge(others: Collection<Modifier>): List<Modifier> =
    others.fold(this) { list, other ->
        list.merge(other)
    }

operator fun State.plus(other: State) =
    State(
        name = "Gesamt",
        modifiers = this.modifiers.merge(other.modifiers)
    )
