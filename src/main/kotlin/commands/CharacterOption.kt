package de.grannath.pardona.commands

import de.grannath.pardona.jooq.tables.references.CHARACTER
import de.grannath.pardona.jooq.tables.references.USER
import discord4j.common.util.Snowflake
import discord4j.core.GatewayDiscordClient
import discord4j.core.event.domain.interaction.ChatInputAutoCompleteEvent
import discord4j.core.`object`.command.ApplicationCommandOption
import discord4j.discordjson.json.ApplicationCommandOptionChoiceData
import discord4j.discordjson.json.ApplicationCommandOptionData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.reactor.mono
import kotlinx.coroutines.withContext
import org.jooq.DSLContext
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono

val characterOption =
    ApplicationCommandOptionData.builder()
        .name("charakter")
        .description("Wähle einen bestimmten Charakter")
        .type(ApplicationCommandOption.Type.STRING.value)
        .required(false)
        .autocomplete(true)
        .build()

val Invocation.characterName: String?
    get() = getStringOptionValue("charakter")

@Component
class CharacterCompletion(
    client: GatewayDiscordClient,
    private val create: DSLContext,
) {
    init {
        client.on(ChatInputAutoCompleteEvent::class.java)
            .flatMap { event ->
                if (event.focusedOption.name == "charakter") {
                    mono { getSuggestions(event.interaction.user.id, event.focusedOption.value.map { it.asString() }.orElse("")) }
                        .flatMap { event.respondWithSuggestions(it) }
                } else {
                    Mono.empty()
                }
            }
            .subscribe()
    }

    private suspend fun getSuggestions(userId: Snowflake, prefix: String): List<ApplicationCommandOptionChoiceData> = withContext(Dispatchers.IO) {
        create.select(CHARACTER.NAME)
            .from(CHARACTER)
            .join(USER).onKey(CHARACTER.USER_ID)
            .where(USER.DISCORD_ID.eq(userId.asString()))
            .and(CHARACTER.NAME.startsWithIgnoreCase(prefix))
            .orderBy(CHARACTER.NAME)
            .limit(5)
            .fetch(CHARACTER.NAME)
            .map {
                ApplicationCommandOptionChoiceData.builder()
                    .name(it!!)
                    .value(it)
                    .build()
            }
    }
}
