package de.grannath.pardona.commands.talents

import de.grannath.pardona.characters.CharacterZauber
import de.grannath.pardona.characters.fetchInto
import de.grannath.pardona.characters.selectCharacter
import de.grannath.pardona.checks.CheckInput
import de.grannath.pardona.checks.check
import de.grannath.pardona.commands.Invocation
import de.grannath.pardona.commands.SlashCommand
import de.grannath.pardona.commands.UserException
import de.grannath.pardona.commands.characterName
import de.grannath.pardona.commands.characterOption
import de.grannath.pardona.commands.fetchEigenschaftInput
import de.grannath.pardona.commands.recipientOption
import de.grannath.pardona.jooq.tables.pojos.ZauberValue
import de.grannath.pardona.jooq.tables.references.CHARACTER
import de.grannath.pardona.jooq.tables.references.ZAUBER_VALUE
import discord4j.common.util.Snowflake
import discord4j.core.GatewayDiscordClient
import discord4j.core.`object`.command.ApplicationCommandOption
import discord4j.discordjson.json.ApplicationCommandOptionChoiceData
import discord4j.discordjson.json.ApplicationCommandOptionData
import discord4j.discordjson.json.ApplicationCommandRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jooq.DSLContext
import org.springframework.stereotype.Component

@Component
class ZauberCommand(
    private val create: DSLContext,
    client: GatewayDiscordClient,
) : SlashCommand(client) {
    override val command: ApplicationCommandRequest =
        ApplicationCommandRequest.builder()
            .name("zauber")
            .description("Wirf eine Zauberprobe.")
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("frei")
                    .description("Wirf eine Zauberprobe mit beliebigen Eigenschaftswerten.")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("eigenschaft-eins")
                            .description("Der erste Eigenschaftswert")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .required(true)
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("eigenschaft-zwei")
                            .description("Der zweite Eigenschaftswert")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .required(true)
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("eigenschaft-drei")
                            .description("Der dritte Eigenschaftswert")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .required(true)
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("zauber")
                            .description("Der Zauberwert, als Zahl oder Name eines Zaubers")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .required(true)
                            .autocomplete(true)
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("modifikator")
                            .description("Eine Erleichterung oder Erschwernis")
                            .type(ApplicationCommandOption.Type.INTEGER.value)
                            .required(false)
                            .build()
                    )
                    .addOption(characterOption)
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("standard")
                    .description("Wirf eine Zauberprobe auf einen bestimmten Zauber.")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("zauber")
                            .description("Der Name des Zaubers")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .required(true)
                            .autocomplete(true)
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("modifikator")
                            .description("Eine Erleichterung oder Erschwernis")
                            .type(ApplicationCommandOption.Type.INTEGER.value)
                            .required(false)
                            .build()
                    )
                    .addOption(characterOption)
                    .addOption(recipientOption)
                    .build()
            )
            .build()

    init {
        autocomplete("zauber") { user, prefix ->
            withContext(Dispatchers.IO) {
                create.select(ZAUBER_VALUE.NAME)
                    .from(ZAUBER_VALUE)
                    .join(CHARACTER).onKey(ZAUBER_VALUE.CHARACTER_ID)
                    .selectCharacter(user.id, null)
                    .and(ZAUBER_VALUE.NAME.startsWithIgnoreCase(prefix))
                    .fetch {
                        ApplicationCommandOptionChoiceData.builder()
                            .name(it.value1()!!)
                            .value(it.value1()!!)
                            .build()
                    }
            }
        }
    }

    override suspend fun handle(event: Invocation) {
        val zauberInput = create.fetchZauberInput(event.getStringOptionValue("zauber")!!, event)

        val response = zauberInput.check().toMessage(event)

        event.reply(response)
    }
}

private suspend fun DSLContext.fetchZauberInput(input: String, invocation: Invocation): CheckInput {
    val zauber = fetchZauber(invocation.user.id, invocation.characterName)
    val candidates = zauber.filter { it.name.startsWith(input, ignoreCase = true) }

    return if (invocation.name == "standard") {
        val characterTalent = when {
            zauber.isEmpty() -> throw UserException("Ich kenne euch leider noch nicht.")
            candidates.isEmpty() -> throw UserException("Ihr habt keinen Zauber $input.")
            candidates.size > 1 -> throw UserException("Welchen eurer vielen Zauber meint ihr? ${candidates.joinToString(separator = ", ", limit = 3) { it.name }}")
            else -> candidates[0]
        }
        val eigenschaften = fetchEigenschaftInput(
            invocation,
            characterTalent.check.first,
            characterTalent.check.second,
            characterTalent.check.third,
            userModifier = false,
        )
        CheckInput.Named(
            talent = characterTalent,
            eigenschaften = Triple(eigenschaften[0], eigenschaften[1], eigenschaften[2]),
            userModifier = invocation.getIntOptionValue("modifikator") ?: 0,
            stateModifier = 0,
        )
    } else {
        val eigenschaften = fetchEigenschaftInput(
            invocation,
            invocation.getStringOptionValue("eigenschaft-eins")!!,
            invocation.getStringOptionValue("eigenschaft-zwei")!!,
            invocation.getStringOptionValue("eigenschaft-drei")!!,
            userModifier = false,
        )
        if (input.toIntOrNull() == null) {
            val characterTalent = when {
                zauber.isEmpty() -> throw UserException("Bitte nennt mir eine Zahl als Zauberwert.")
                candidates.isEmpty() -> throw UserException("Ihr habt keinen Zauber $input.")
                candidates.size > 1 -> throw UserException("Welchen eurer vielen Zauber meint ihr? ${candidates.joinToString(separator = ", ", limit = 3) { it.name }}")
                else -> candidates[0]
            }
            CheckInput.Named(
                talent = characterTalent,
                eigenschaften = Triple(eigenschaften[0], eigenschaften[1], eigenschaften[2]),
                userModifier = invocation.getIntOptionValue("modifikator") ?: 0,
                stateModifier = 0,
            )
        } else {
            CheckInput.Number(
                rawValue = input.toInt(),
                eigenschaften = Triple(eigenschaften[0], eigenschaften[1], eigenschaften[2]),
                modifier = invocation.getIntOptionValue("modifikator") ?: 0,
            )
        }
    }
}

private suspend fun DSLContext.fetchZauber(discordId: Snowflake, characterName: String?): List<CharacterZauber> =
    withContext(Dispatchers.IO) {
        select(ZAUBER_VALUE.asterisk())
            .from(ZAUBER_VALUE)
            .join(CHARACTER)
            .on(ZAUBER_VALUE.CHARACTER_ID.eq(CHARACTER.ID))
            .selectCharacter(discordId, characterName)
            .fetchInto<ZauberValue>()
            .map {
                CharacterZauber(
                    name = it.name!!,
                    value = it.value!!,
                    check = it.checkString!!
                        .removeSurrounding("(", ")")
                        .split("/")
                        .let { Triple(it[0], it[1], it[2]) }
                )
            }
    }
