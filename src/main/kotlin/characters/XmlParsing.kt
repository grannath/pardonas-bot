package de.grannath.pardona.characters

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.w3c.dom.Document
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import java.io.StringWriter
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.OutputKeys
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult
import kotlin.math.roundToInt

private val eigenschaften = listOf(
    "Mut",
    "Klugheit",
    "Intuition",
    "Charisma",
    "Fingerfertigkeit",
    "Gewandtheit",
    "Konstitution",
    "Körperkraft"
)

private val builderFactory: DocumentBuilderFactory = DocumentBuilderFactory.newInstance()
private val transformerFactory = TransformerFactory.newInstance()

suspend fun parseDocument(contents: String) =
    withContext(Dispatchers.IO) {
        builderFactory.newDocumentBuilder().parse(
            contents.byteInputStream()
        )
    }

fun Document.serialize(): String =
    StringWriter().also { stringWriter ->
        transformerFactory.newTransformer().apply {
            setOutputProperty(OutputKeys.METHOD, "xml")
            setOutputProperty(OutputKeys.INDENT, "false")
            setOutputProperty(OutputKeys.ENCODING, "UTF-8")
        }.transform(DOMSource(this), StreamResult(stringWriter))
    }.toString()

fun Document.getName() =
    getElementsByTagName("held")
        .item(0)
        .getStringAttribute("name")

fun Document.getCharacterStatus() =
    getEigenschaften().let { eigenschaften ->
        CharacterStatus(
            maxLep = getElementsByTagName("eigenschaft")
                .asList()
                .single { it.getStringAttribute("name") == "Lebensenergie" }
                .let { it.getStringAttribute("value").toInt() + it.getStringAttribute("mod").toInt() + eigenschaften.lepBase() },
            maxAup = getElementsByTagName("eigenschaft")
                .asList()
                .single { it.getStringAttribute("name") == "Ausdauer" }
                .let { it.getStringAttribute("value").toInt() + it.getStringAttribute("mod").toInt() + eigenschaften.auBase() },
            maxAsp = getElementsByTagName("eigenschaft")
                .asList()
                .singleOrNull { it.getStringAttribute("name") == "Astralenergie" }
                ?.let { it.getStringAttribute("value").toInt() + it.getStringAttribute("mod").toInt() + it.getStringAttribute("grossemeditation").toInt() + eigenschaften.aspBase(hasSonderfertigkeit("Gefäß der Sterne")) }
                ?.let { if (it == eigenschaften.aspBase(hasSonderfertigkeit("Gefäß der Sterne"))) null else it },
            maxKap = getElementsByTagName("eigenschaft")
                .asList()
                .singleOrNull { it.getStringAttribute("name") == "Karmaenergie" }
                ?.let { it.getStringAttribute("value").toInt() + it.getStringAttribute("mod").toInt() + it.getStringAttribute("karmalqueste").toInt() }
                ?.let { if (it == 0) null else it }
        )
    }

private fun Document.hasSonderfertigkeit(name: String) =
    getElementsByTagName("sonderfertigkeit")
        .asList()
        .any { name == it.getStringAttribute("name").trim() }

private fun List<CharacterEigenschaft>.getValue(name: String) =
    single { it.shortForm.equals(name, ignoreCase = true) }.value

private fun List<CharacterEigenschaft>.lepBase(): Int =
    ((getValue("KO") + getValue("KO") + getValue("KK")) / 2.0).roundToInt()

private fun List<CharacterEigenschaft>.auBase(): Int =
    ((getValue("MU") + getValue("KO") + getValue("GE")) / 2.0).roundToInt()

private fun List<CharacterEigenschaft>.aspBase(gefaessDerSterne: Boolean): Int =
    if (gefaessDerSterne)
        ((getValue("MU") + getValue("IN") + 2 * getValue("CH")) / 2.0).roundToInt()
    else
        ((getValue("MU") + getValue("IN") + getValue("CH")) / 2.0).roundToInt()

fun Document.getEigenschaften() =
    getElementsByTagName("eigenschaft")
        .asList()
        .filter { eigenschaften.contains(it.getStringAttribute("name").trim()) }
        .map {
            CharacterEigenschaft(
                name = it.getStringAttribute("name").trim(),
                value = it.getStringAttribute("value").toInt() + it.getStringAttribute("mod").toInt()
            )
        }

fun Document.getTalente() =
    getElementsByTagName("talent")
        .asList()
        .filter { it.attributes.getNamedItem("value") != null }
        .map {
            CharacterTalent(
                name = it.getStringAttribute("name").trim(),
                value = it.getStringAttribute("value").toInt(),
                check = it.getStringAttribute("probe").trim()
                    .removeSurrounding("(", ")")
                    .split("/")
                    .let { Triple(it[0], it[1], it[2]) }
            )
        }

fun Document.getZauber() =
    getElementsByTagName("zauber")
        .asList()
        .filter { it.attributes.getNamedItem("value") != null }
        .map {
            CharacterZauber(
                name = it.getStringAttribute("name").trim(),
                value = it.getStringAttribute("value").toInt(),
                check = it.getStringAttribute("probe").trim()
                    .removeSurrounding("(", ")")
                    .split("/")
                    .let { Triple(it[0], it[1], it[2]) }
            )
        }

private fun NodeList.asList(): List<Node> =
    mutableListOf<Node>().also {
        for (i in 0 until this.length) {
            it.add(this.item(i))
        }
    }

private fun Node.getStringAttribute(name: String): String =
    this.attributes.getNamedItem(name).nodeValue
