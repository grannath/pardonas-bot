package de.grannath.pardona.commands.talents

import de.grannath.pardona.characters.CharacterTalent
import de.grannath.pardona.characters.fetchInto
import de.grannath.pardona.characters.selectCharacter
import de.grannath.pardona.checks.CheckInput
import de.grannath.pardona.checks.check
import de.grannath.pardona.commands.Invocation
import de.grannath.pardona.commands.SlashCommand
import de.grannath.pardona.commands.UserException
import de.grannath.pardona.commands.characterName
import de.grannath.pardona.commands.characterOption
import de.grannath.pardona.commands.fetchEigenschaftInput
import de.grannath.pardona.commands.recipientOption
import de.grannath.pardona.jooq.tables.pojos.TalentValue
import de.grannath.pardona.jooq.tables.references.CHARACTER
import de.grannath.pardona.jooq.tables.references.TALENT_VALUE
import discord4j.common.util.Snowflake
import discord4j.core.GatewayDiscordClient
import discord4j.core.`object`.command.ApplicationCommandOption
import discord4j.discordjson.json.ApplicationCommandOptionChoiceData
import discord4j.discordjson.json.ApplicationCommandOptionData
import discord4j.discordjson.json.ApplicationCommandRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jooq.DSLContext
import org.springframework.stereotype.Component

@Component
class TalentCommand(
    private val create: DSLContext,
    client: GatewayDiscordClient,
) : SlashCommand(client) {
    override val command: ApplicationCommandRequest =
        ApplicationCommandRequest.builder()
            .name("talent")
            .description("Wirf eine Talentprobe.")
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("frei")
                    .description("Wirf eine Talentprobe mit beliebigen Eigenschaftswerten.")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("eigenschaft-eins")
                            .description("Der erste Eigenschaftswert")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .required(true)
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("eigenschaft-zwei")
                            .description("Der zweite Eigenschaftswert")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .required(true)
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("eigenschaft-drei")
                            .description("Der dritte Eigenschaftswert")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .required(true)
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("talent")
                            .description("Der Talentwert, als Zahl oder Name eines Talents")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .required(true)
                            .autocomplete(true)
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("modifikator")
                            .description("Eine Erleichterung oder Erschwernis")
                            .type(ApplicationCommandOption.Type.INTEGER.value)
                            .required(false)
                            .build()
                    )
                    .addOption(characterOption)
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("standard")
                    .description("Wirf eine Talentprobe auf ein bestimmtes Talent.")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("talent")
                            .description("Der Name des Talents")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .required(true)
                            .autocomplete(true)
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("modifikator")
                            .description("Eine Erleichterung oder Erschwernis")
                            .type(ApplicationCommandOption.Type.INTEGER.value)
                            .required(false)
                            .build()
                    )
                    .addOption(characterOption)
                    .addOption(recipientOption)
                    .build()
            )
            .build()

    init {
        autocomplete("talent") { user, prefix ->
            if (prefix.isNullOrBlank())
                emptyList()
            else
                withContext(Dispatchers.IO) {
                    create.select(TALENT_VALUE.NAME)
                        .from(TALENT_VALUE)
                        .join(CHARACTER).onKey(TALENT_VALUE.CHARACTER_ID)
                        .selectCharacter(user.id, null)
                        .and(TALENT_VALUE.NAME.startsWithIgnoreCase(prefix))
                        .fetch {
                            ApplicationCommandOptionChoiceData.builder()
                                .name(it.value1()!!)
                                .value(it.value1()!!)
                                .build()
                        }
                }
        }
    }

    override suspend fun handle(event: Invocation) {
        val talentInput = create.fetchTalentInput(event.getStringOptionValue("talent")!!, event)

        val response = talentInput.check().toMessage(event)

        event.reply(response)
    }
}

private suspend fun DSLContext.fetchTalentInput(input: String, invocation: Invocation): CheckInput {
    val talente = fetchTalente(invocation.user.id, invocation.characterName)
    val candidates = talente.filter { it.name.startsWith(input, ignoreCase = true) }

    return if (invocation.name == "standard") {
        val characterTalent = when {
            talente.isEmpty() -> throw UserException("Ich kenne euch leider noch nicht.")
            candidates.isEmpty() -> throw UserException("Ihr habt kein Talent $input.")
            candidates.size > 1 -> throw UserException("Welches eurer vielen Talente meint ihr? ${candidates.joinToString(separator = ", ", limit = 3) { it.name }}")
            else -> candidates[0]
        }
        val eigenschaften = fetchEigenschaftInput(
            invocation,
            characterTalent.check.first,
            characterTalent.check.second,
            characterTalent.check.third,
            userModifier = false,
        )
        CheckInput.Named(
            talent = characterTalent,
            eigenschaften = Triple(eigenschaften[0], eigenschaften[1], eigenschaften[2]),
            userModifier = invocation.getIntOptionValue("modifikator") ?: 0,
            stateModifier = 0,
        )
    } else {
        val eigenschaften = fetchEigenschaftInput(
            invocation,
            invocation.getStringOptionValue("eigenschaft-eins")!!,
            invocation.getStringOptionValue("eigenschaft-zwei")!!,
            invocation.getStringOptionValue("eigenschaft-drei")!!,
            userModifier = false,
        )
        if (input.toIntOrNull() == null) {
            val characterTalent = when {
                talente.isEmpty() -> throw UserException("Bitte nennt mir eine Zahl als Talentwert.")
                candidates.isEmpty() -> throw UserException("Ihr habt kein Talent $input.")
                candidates.size > 1 -> throw UserException("Welches eurer vielen Talente meint ihr? ${candidates.joinToString(separator = ", ", limit = 3) { it.name }}")
                else -> candidates[0]
            }
            CheckInput.Named(
                talent = characterTalent,
                eigenschaften = Triple(eigenschaften[0], eigenschaften[1], eigenschaften[2]),
                userModifier = invocation.getIntOptionValue("modifikator") ?: 0,
                stateModifier = 0,
            )
        } else {
            CheckInput.Number(
                rawValue = input.toInt(),
                eigenschaften = Triple(eigenschaften[0], eigenschaften[1], eigenschaften[2]),
                modifier = invocation.getIntOptionValue("modifikator") ?: 0,
            )
        }
    }
}

private suspend fun DSLContext.fetchTalente(discordId: Snowflake, characterName: String?): List<CharacterTalent> = withContext(Dispatchers.IO) {
    select(TALENT_VALUE.asterisk())
        .from(TALENT_VALUE)
        .join(CHARACTER)
        .on(TALENT_VALUE.CHARACTER_ID.eq(CHARACTER.ID))
        .selectCharacter(discordId, characterName)
        .fetchInto<TalentValue>()
        .map {
            CharacterTalent(
                name = it.name!!,
                value = it.value!!,
                check = it.checkString!!
                    .removeSurrounding("(", ")")
                    .split("/")
                    .let { Triple(it[0], it[1], it[2]) }
            )
        }
}
