package de.grannath.pardona.checks

import de.grannath.pardona.characters.CharacterEigenschaft
import de.grannath.pardona.commands.UserException

sealed interface EigenschaftInput {
    val rawValue: Int
    val modifier: Int
    val effectiveValue: Int
        get() = rawValue - modifier

    data class Named(
        val eigenschaft: CharacterEigenschaft,
        val userModifier: Int,
        val stateModifier: Int,
    ) : EigenschaftInput {
        override val rawValue: Int = eigenschaft.value
        override val modifier: Int = userModifier - stateModifier

        init {
            if (modifier !in -40..40) {
                throw UserException("Eine Erschwernis oder Erleichterung muss zwischen -40 und 40 liegen.")
            }
        }
    }

    data class Number(
        override val rawValue: Int,
        override val modifier: Int,
    ) : EigenschaftInput {
        init {
            if (rawValue !in 1..34) {
                throw UserException("Eine Eigenschaft muss zwischen 0 und 35 sein.")
            }
            if (modifier !in -40..40) {
                throw UserException("Eine Erschwernis oder Erleichterung muss zwischen -40 und 40 liegen.")
            }
        }
    }
}

sealed interface EigenschaftCheckResult {
    val input: EigenschaftInput

    data class CriticalSuccess(
        override val input: EigenschaftInput,
        val secondRoll: Int,
        val maxAddModifier: Int?
    ) : EigenschaftCheckResult

    data class CriticalChanceSuccess(
        override val input: EigenschaftInput,
        val secondRoll: Int,
        val maxModifier: Int?
    ) : EigenschaftCheckResult

    data class CriticalFailure(
        override val input: EigenschaftInput,
        val secondRoll: Int,
        val maxModifier: Int?
    ) : EigenschaftCheckResult

    data class CriticalChanceFailure(
        override val input: EigenschaftInput,
        val secondRoll: Int,
        val maxAddModifier: Int?
    ) : EigenschaftCheckResult

    data class Success(
        override val input: EigenschaftInput,
        val roll: Int,
        val maxAddModifier: Int
    ) : EigenschaftCheckResult

    data class Failure(
        override val input: EigenschaftInput,
        val roll: Int,
        val maxModifier: Int
    ) : EigenschaftCheckResult
}

fun EigenschaftCheckResult.isSuccess() =
    when (this) {
        is EigenschaftCheckResult.CriticalSuccess -> true
        is EigenschaftCheckResult.CriticalChanceSuccess -> true
        is EigenschaftCheckResult.Success -> true
        else -> false
    }

fun EigenschaftInput.check(rollSource: RollSource = defaultRollSource): EigenschaftCheckResult {
    val first = roll(20, rollSource)
    val second = roll(20, rollSource)

    return when {
        first == 1 && compare(second) ->
            EigenschaftCheckResult.CriticalSuccess(this, second, maxAddModifier(second))

        first == 1 ->
            EigenschaftCheckResult.CriticalChanceSuccess(this, second, maxModifier(second))

        first == 20 && compare(second) ->
            EigenschaftCheckResult.CriticalChanceFailure(this, second, maxAddModifier(second))

        first == 20 ->
            EigenschaftCheckResult.CriticalFailure(this, second, maxModifier(second))

        compare(first) ->
            EigenschaftCheckResult.Success(this, first, maxAddModifier(first)!!)

        else ->
            EigenschaftCheckResult.Failure(this, first, maxModifier(first)!!)
    }
}

internal fun EigenschaftInput.compare(roll: Int) =
    roll == 1 || (roll != 20 && roll <= effectiveValue)

internal fun EigenschaftInput.maxAddModifier(roll: Int) =
    if (roll == 20 || roll == 1) null else effectiveValue - roll

internal fun EigenschaftInput.maxModifier(roll: Int) =
    if (roll == 20 || roll == 1) null else rawValue - roll
