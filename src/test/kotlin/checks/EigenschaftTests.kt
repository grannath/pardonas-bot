package de.grannath.pardona.checks

import io.kotest.assertions.fail
import io.kotest.core.spec.style.FreeSpec
import io.kotest.datatest.withData
import io.kotest.matchers.shouldBe

class EigenschaftTests : FreeSpec({
    fun rollSource(vararg values: Int): RollSource =
        values.toMutableList().let {
            { _: Int -> if (it.isNotEmpty()) it.removeAt(0) else 0 }
        }

    "Eigenschaft.check() should" - {
        "always return CriticalSuccess after 1 and successful roll" - {
            withData(
                TestCase(15, 0, 15, 0),
                TestCase(15, 2, 13, 0),
                TestCase(15, -3, 18, 0),
                TestCase(15, 0, 1, null),
                TestCase(10, 12, 1, null),
                TestCase(8, -3, 10, 1),
                TestCase(15, 2, 10, 3)
            ) { (e, m, c, mam) ->
                EigenschaftInput.Number(e, m).check(rollSource(1, c)) shouldBe EigenschaftCheckResult.CriticalSuccess(EigenschaftInput.Number(e, m), c, mam)
            }
        }

        "always return CriticalChanceSuccess after 1 and unsuccessful roll" - {
            withData(
                TestCase(15, 0, 17, -2),
                TestCase(15, 4, 13, 2),
                TestCase(15, 2, 15, 0),
                TestCase(15, -3, 19, -4),
                TestCase(15, 0, 20, null),
                TestCase(10, -12, 20, null),
                TestCase(8, -3, 14, -6),
                TestCase(15, 2, 16, -1)
            ) { (e, m, c, mam) ->
                EigenschaftInput.Number(e, m).check(rollSource(1, c)) shouldBe EigenschaftCheckResult.CriticalChanceSuccess(EigenschaftInput.Number(e, m), c, mam)
            }
        }

        "always return CriticalFailure after 20 and unsuccessful roll" - {
            withData(
                TestCase(15, 0, 17, -2),
                TestCase(15, 4, 13, 2),
                TestCase(15, 2, 15, 0),
                TestCase(15, -3, 19, -4),
                TestCase(15, 0, 20, null),
                TestCase(10, -12, 20, null),
                TestCase(8, -3, 14, -6),
                TestCase(15, 2, 16, -1)
            ) { (e, m, c, mam) ->
                EigenschaftInput.Number(e, m).check(rollSource(20, c)) shouldBe EigenschaftCheckResult.CriticalFailure(EigenschaftInput.Number(e, m), c, mam)
            }
        }

        "should always return CriticalChanceFailure after 20 and successful roll" - {
            withData(
                TestCase(15, 0, 15, 0),
                TestCase(15, 2, 13, 0),
                TestCase(15, -3, 18, 0),
                TestCase(15, 0, 1, null),
                TestCase(10, 12, 1, null),
                TestCase(8, -3, 10, 1),
                TestCase(15, 2, 10, 3)
            ) { (e, m, c, mam) ->
                EigenschaftInput.Number(e, m).check(rollSource(20, c)) shouldBe EigenschaftCheckResult.CriticalChanceFailure(EigenschaftInput.Number(e, m), c, mam)
            }
        }

        "should always return Success a successful roll" - {
            withData(
                TestCase(15, 0, 15, 0),
                TestCase(15, 2, 13, 0),
                TestCase(15, -3, 18, 0),
                TestCase(8, -3, 10, 1),
                TestCase(15, 2, 10, 3)
            ) { (e, m, c, mam) ->
                EigenschaftInput.Number(e, m).check(rollSource(c)) shouldBe EigenschaftCheckResult.Success(EigenschaftInput.Number(e, m), c, mam ?: fail("Wrong test input"))
            }
        }

        "should always return Failure after an unsuccessful roll" - {
            withData(
                TestCase(15, 0, 17, -2),
                TestCase(15, 4, 13, 2),
                TestCase(15, 2, 15, 0),
                TestCase(15, -3, 19, -4),
                TestCase(8, -3, 14, -6),
                TestCase(15, 2, 16, -1)
            ) { (e, m, c, mam) ->
                EigenschaftInput.Number(e, m).check(rollSource(c)) shouldBe EigenschaftCheckResult.Failure(EigenschaftInput.Number(e, m), c, mam ?: fail("Wrong test input"))
            }
        }
    }
}) {
    data class TestCase(
        val eigenschaft: Int,
        val malus: Int,
        val confirmationRoll: Int,
        val maxModifier: Int?
    )
}
