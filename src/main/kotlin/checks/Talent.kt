package de.grannath.pardona.checks

import de.grannath.pardona.characters.CheckValue
import de.grannath.pardona.commands.UserException
import kotlin.math.max
import kotlin.math.min

sealed interface CheckInput {
    val rawValue: Int
    val eigenschaften: Triple<EigenschaftInput, EigenschaftInput, EigenschaftInput>
    val modifier: Int

    val effectiveValue: Int
        get() = rawValue - modifier

    data class Named(
        val talent: CheckValue,
        override val eigenschaften: Triple<EigenschaftInput, EigenschaftInput, EigenschaftInput>,
        val userModifier: Int,
        val stateModifier: Int,
    ) : CheckInput {
        override val rawValue: Int = talent.value
        override val modifier: Int = userModifier - stateModifier

        init {
            if (modifier !in -40..40) {
                throw UserException("Eine Erschwernis oder Erleichterung muss zwischen -40 und 40 liegen.")
            }
        }
    }

    data class Number(
        override val rawValue: Int,
        override val eigenschaften: Triple<EigenschaftInput, EigenschaftInput, EigenschaftInput>,
        override val modifier: Int,
    ) : CheckInput {
        init {
            if (rawValue !in -34..34) {
                throw UserException("Ein Talent muss zwischen -34 und 34 sein.")
            }
            if (modifier !in -40..40) {
                throw UserException("Eine Erschwernis oder Erleichterung muss zwischen -40 und 40 liegen.")
            }
        }
    }
}

private typealias Rolls = Triple<Int, Int, Int>

sealed interface TalentCheckResult {
    val input: CheckInput

    data class EpicSuccess(
        override val input: CheckInput,
        val tapStar: Int
    ) : TalentCheckResult

    data class CriticalSuccess(
        override val input: CheckInput,
        val firstRoll: Int,
        val secondRoll: Int,
        val thirdRoll: Int,
        val tapStar: Int
    ) : TalentCheckResult

    data class CriticalFailure(
        override val input: CheckInput,
        val firstRoll: Int,
        val secondRoll: Int,
        val thirdRoll: Int
    ) : TalentCheckResult

    data class Success(
        override val input: CheckInput,
        val firstRoll: Int,
        val secondRoll: Int,
        val thirdRoll: Int,
        val tapStar: Int,
        val maxAddModifier: Int
    ) : TalentCheckResult

    data class Failure(
        override val input: CheckInput,
        val firstRoll: Int,
        val secondRoll: Int,
        val thirdRoll: Int,
        val maxModifier: Int
    ) : TalentCheckResult
}

internal fun rollThree(rollSource: RollSource) =
    Triple(
        roll(20, rollSource),
        roll(20, rollSource),
        roll(20, rollSource)
    )

fun CheckInput.check(rollSource: RollSource = defaultRollSource) =
    rollThree(rollSource).let { rolls ->
        when {
            rolls == Triple(1, 1, 1) ->
                TalentCheckResult.EpicSuccess(this, max(1, effectiveValue))
            rolls.toList().count { it == 1 } == 2 ->
                TalentCheckResult.CriticalSuccess(
                    this,
                    rolls.first,
                    rolls.second,
                    rolls.third,
                    max(1, effectiveValue)
                )
            rolls.toList().count { it == 20 } >= 2 ->
                TalentCheckResult.CriticalFailure(
                    this,
                    rolls.first,
                    rolls.second,
                    rolls.third
                )
            else ->
                calculateSuccess(rolls)
        }
    }

internal fun CheckInput.calculateSuccess(rolls: Rolls): TalentCheckResult =
    if (effectiveValue >= 0) {
        calculatePositivePool(rolls)
    } else {
        calculateNegativePool(rolls)
    }

internal fun CheckInput.calculatePositivePool(rolls: Rolls) =
    when {
        necessaryPoints(rolls) <= effectiveValue ->
            TalentCheckResult.Success(
                this,
                rolls.first,
                rolls.second,
                rolls.third,
                min(rawValue, max(1, effectiveValue - necessaryPoints(rolls))),
                maxModifier(rolls) - modifier
            )
        else ->
            TalentCheckResult.Failure(
                this,
                rolls.first,
                rolls.second,
                rolls.third,
                maxModifier(rolls)
            )
    }

internal fun CheckInput.calculateNegativePool(rolls: Rolls) =
    when (
        val result = CheckInput.Number(
            rawValue = 0,
            modifier = 0,
            eigenschaften = Triple(
                EigenschaftInput.Number(
                    eigenschaften.first.effectiveValue + effectiveValue,
                    0,
                ),
                EigenschaftInput.Number(
                    eigenschaften.second.effectiveValue + effectiveValue,
                    0,
                ),
                EigenschaftInput.Number(
                    eigenschaften.third.effectiveValue + effectiveValue,
                    0,
                ),
            ),
        ).calculatePositivePool(rolls)
    ) {
        is TalentCheckResult.Success -> TalentCheckResult.Success(
            this,
            rolls.first,
            rolls.second,
            rolls.third,
            1,
            result.maxAddModifier
        )
        is TalentCheckResult.Failure -> TalentCheckResult.Failure(
            this,
            rolls.first,
            rolls.second,
            rolls.third,
            result.maxModifier
        )
        else -> throw IllegalStateException("Should only be called on non-critical result, but result was $result.")
    }

internal fun CheckInput.maxModifier(rolls: Rolls) =
    if (necessaryPoints(rolls) == 0) {
        minDifference(rolls) + rawValue
    } else {
        rawValue - necessaryPoints(rolls)
    }

internal fun CheckInput.minDifference(rolls: Rolls): Int =
    max(
        0,
        listOf(
            eigenschaften.first.effectiveValue - rolls.first,
            eigenschaften.second.effectiveValue - rolls.second,
            eigenschaften.third.effectiveValue - rolls.third
        ).minOrNull()!!
    )

internal fun CheckInput.necessaryPoints(rolls: Rolls) =
    necessaryPoints(eigenschaften.first.effectiveValue, rolls.first) +
        necessaryPoints(eigenschaften.second.effectiveValue, rolls.second) +
        necessaryPoints(eigenschaften.third.effectiveValue, rolls.third)

internal fun necessaryPoints(eigenschaft: Int, roll: Int) =
    if (roll <= eigenschaft) 0 else roll - eigenschaft
