CREATE TABLE "inventory"
(
    "id"           BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    "character_id" BIGINT NOT NULL,
    "money"        BIGINT NOT NULL CHECK (money >= 0) DEFAULT 0
);

ALTER TABLE "inventory"
    ADD CONSTRAINT "FK_inventory_character_id"
        FOREIGN KEY ("character_id") REFERENCES "character" ("id");

ALTER TABLE "inventory"
    ADD CONSTRAINT "UNQ_inventory_character_id"
        UNIQUE ("character_id");
