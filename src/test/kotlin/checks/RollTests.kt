package de.grannath.pardona.checks

import io.kotest.core.spec.style.FreeSpec
import io.kotest.datatest.withData
import io.kotest.matchers.shouldBe

class RollTests : FreeSpec({
    "rollExpression() should" - {
        fun rollSource(values: List<Int>): RollSource =
            values.toMutableList().let {
                { _: Int -> if (it.isNotEmpty()) it.removeAt(0) else 0 }
            }

        "handle single roll expressions" - {
            withData(
                TestCase("1w6", listOf(3), 3),
                TestCase("2d3", listOf(2, 3), 5),
                TestCase("5d7", listOf(2, 3, 1, 4, 5), 15)
            ) { (exp, rolls, res) ->
                rollExpression(exp, rollSource(rolls)) shouldBe res
            }
        }

        "handle single constant expressions" - {
            withData(
                TestCase("3", listOf(), 3),
                TestCase("5", listOf(), 5),
                TestCase("15", listOf(), 15)
            ) { (exp, _, res) ->
                rollExpression(exp) shouldBe res
            }
        }

        "handle complex expressions" - {
            withData(
                TestCase("1w6+3", listOf(3), 6),
                TestCase("2d3 - 4", listOf(2, 3), 1),
                TestCase("5d7+ 2d3 -1", listOf(2, 3, 1, 4, 5, 1, 1), 16),
                TestCase("5d7 - 2w4", listOf(2, 3, 1, 4, 5, 1, 4), 10)
            ) { (exp, rolls, res) ->
                rollExpression(exp, rollSource(rolls)) shouldBe res
            }
        }
    }
}) {
    data class TestCase(
        val expression: String,
        val rolls: List<Int>,
        val result: Int
    )
}
