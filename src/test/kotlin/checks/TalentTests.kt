package de.grannath.pardona.checks

import de.grannath.pardona.checks.TalentCheckResult.CriticalFailure
import de.grannath.pardona.checks.TalentCheckResult.CriticalSuccess
import de.grannath.pardona.checks.TalentCheckResult.EpicSuccess
import de.grannath.pardona.checks.TalentCheckResult.Failure
import de.grannath.pardona.checks.TalentCheckResult.Success
import io.kotest.core.spec.style.FreeSpec
import io.kotest.core.spec.style.scopes.ContainerScope
import io.kotest.datatest.withData
import io.kotest.matchers.shouldBe

class TalentTests : FreeSpec({
    "TalentInput.check() should" - {
        "return EpicSuccess when rolling three 1s" - {
            talentChecksWorkFor(
                TestCase(12, 15, 10, 5, 0, 1, 1, 1, EpicSuccess(checkInput(12, 15, 10, 5, 0), 5)),
                TestCase(8, 20, 11, 3, 2, 1, 1, 1, EpicSuccess(checkInput(8, 20, 11, 3, 2), 1)),
                TestCase(8, 20, 11, 3, 1, 1, 1, 1, EpicSuccess(checkInput(8, 20, 11, 3, 1), 2)),
                TestCase(8, 6, 20, 12, 5, 1, 1, 1, EpicSuccess(checkInput(8, 6, 20, 12, 5), 7)),
                TestCase(8, 6, 20, 0, 0, 1, 1, 1, EpicSuccess(checkInput(8, 6, 20, 0, 0), 1)),
                TestCase(1, 1, 1, 25, 10, 1, 1, 1, EpicSuccess(checkInput(1, 1, 1, 25, 10), 15))
            )
        }

        "return CriticalSuccess when rolling two 1s" - {
            talentChecksWorkFor(
                TestCase(12, 15, 10, 5, 0, 1, 7, 1, CriticalSuccess(checkInput(12, 15, 10, 5, 0), 1, 7, 1, 5)),
                TestCase(8, 20, 11, 3, 2, 20, 1, 1, CriticalSuccess(checkInput(8, 20, 11, 3, 2), 20, 1, 1, 1)),
                TestCase(8, 20, 11, 3, 1, 1, 1, 17, CriticalSuccess(checkInput(8, 20, 11, 3, 1), 1, 1, 17, 2)),
                TestCase(8, 6, 20, 12, 5, 12, 1, 1, CriticalSuccess(checkInput(8, 6, 20, 12, 5), 12, 1, 1, 7)),
                TestCase(8, 6, 20, 0, 0, 5, 1, 1, CriticalSuccess(checkInput(8, 6, 20, 0, 0), 5, 1, 1, 1)),
                TestCase(1, 1, 1, 25, 10, 1, 18, 1, CriticalSuccess(checkInput(1, 1, 1, 25, 10), 1, 18, 1, 15))
            )
        }

        "return CriticalFailure when rolling two or three 20s" - {
            talentChecksWorkFor(
                TestCase(12, 15, 10, 5, 0, 20, 7, 20, CriticalFailure(checkInput(12, 15, 10, 5, 0), 20, 7, 20)),
                TestCase(8, 20, 11, 3, 2, 20, 20, 1, CriticalFailure(checkInput(8, 20, 11, 3, 2), 20, 20, 1)),
                TestCase(8, 20, 11, 3, 1, 1, 20, 20, CriticalFailure(checkInput(8, 20, 11, 3, 1), 1, 20, 20)),
                TestCase(11, 12, 20, 12, 0, 20, 10, 20, CriticalFailure(checkInput(11, 12, 20, 12, 0), 20, 10, 20)),
                TestCase(8, 6, 20, 0, 0, 5, 20, 20, CriticalFailure(checkInput(8, 6, 20, 0, 0), 5, 20, 20)),
                TestCase(1, 1, 1, 25, 10, 20, 20, 20, CriticalFailure(checkInput(1, 1, 1, 25, 10), 20, 20, 20))
            )
        }

        "return full points when rolling lower than the attributes" - {
            talentChecksWorkFor(
                TestCase(12, 15, 10, 5, 0, 10, 7, 10, Success(checkInput(12, 15, 10, 5, 0), 10, 7, 10, 5, 5)),
                TestCase(8, 20, 11, 3, 2, 5, 20, 9, Success(checkInput(8, 20, 11, 3, 2), 5, 20, 9, 1, 1)),
                TestCase(8, 20, 11, 3, -3, 5, 20, 9, Success(checkInput(8, 20, 11, 3, -3), 5, 20, 9, 3, 6)),
                TestCase(8, 20, 11, 3, 5, 5, 18, 9, Success(checkInput(8, 20, 11, 3, 5), 5, 18, 9, 1, 0)),
                TestCase(22, 14, 11, 14, 7, 18, 9, 9, Success(checkInput(22, 14, 11, 14, 7), 18, 9, 9, 7, 9)),
                TestCase(22, 14, 11, 14, 14, 18, 9, 9, Success(checkInput(22, 14, 11, 14, 14), 18, 9, 9, 1, 2)),
                TestCase(22, 14, 11, 14, 16, 18, 9, 9, Success(checkInput(22, 14, 11, 14, 16), 18, 9, 9, 1, 0))
            )
        }

        "return reduced points when rolling within talent point range" - {
            talentChecksWorkFor(
                TestCase(12, 15, 10, 5, 0, 15, 7, 10, Success(checkInput(12, 15, 10, 5, 0), 15, 7, 10, 2, 2)),
                TestCase(8, 20, 11, 3, 2, 9, 20, 11, Success(checkInput(8, 20, 11, 3, 2), 9, 20, 11, 1, 0)),
                TestCase(22, 14, 11, 14, 7, 18, 19, 9, Success(checkInput(22, 14, 11, 14, 7), 18, 19, 9, 2, 2)),
                TestCase(12, 9, 13, 10, -3, 14, 11, 8, Success(checkInput(12, 9, 13, 10, -3), 14, 11, 8, 9, 9)),
                TestCase(12, 9, 13, 10, 3, 14, 11, 8, Success(checkInput(12, 9, 13, 10, 3), 14, 11, 8, 3, 3)),
                TestCase(12, 9, 13, 10, -5, 14, 11, 8, Success(checkInput(12, 9, 13, 10, -5), 14, 11, 8, 10, 11))
            )
        }

        "return failure when rolling outside talent point range" - {
            talentChecksWorkFor(
                TestCase(12, 15, 10, 5, 0, 15, 7, 14, Failure(checkInput(12, 15, 10, 5, 0), 15, 7, 14, -2)),
                TestCase(8, 20, 11, 3, 2, 10, 20, 11, Failure(checkInput(8, 20, 11, 3, 2), 10, 20, 11, 1)),
                TestCase(22, 14, 11, 14, 7, 18, 19, 19, Failure(checkInput(22, 14, 11, 14, 7), 18, 19, 19, 1)),
                TestCase(12, 9, 13, 10, -3, 15, 15, 19, Failure(checkInput(12, 9, 13, 10, -3), 15, 15, 19, -5)),
                TestCase(12, 9, 13, 10, 3, 12, 15, 19, Failure(checkInput(12, 9, 13, 10, 3), 12, 15, 19, -2))
            )
        }
    }
}) {
    data class TestCase(
        val eigenschaftOne: Int,
        val eigenschaftTwo: Int,
        val eigenschaftThree: Int,
        val talent: Int,
        val malus: Int,
        val firstRoll: Int,
        val secondRoll: Int,
        val thirdRoll: Int,
        val result: TalentCheckResult
    )
}

private fun rollSource(vararg values: Int): RollSource =
    values.toMutableList().let {
        { _: Int -> it.removeAt(0) }
    }

private suspend fun ContainerScope.talentChecksWorkFor(
    vararg cases: TalentTests.TestCase
): Unit =
    withData(cases.asList()) { (e1, e2, e3, t, m, r1, r2, r3, res) ->
        checkInput(e1, e2, e3, t, m).check(rollSource(r1, r2, r3)) shouldBe res
    }

private fun checkInput(e1: Int, e2: Int, e3: Int, t: Int, m: Int) =
    CheckInput.Number(
        t,
        Triple(EigenschaftInput.Number(e1, 0), EigenschaftInput.Number(e2, 0), EigenschaftInput.Number(e3, 0)),
        m
    )
