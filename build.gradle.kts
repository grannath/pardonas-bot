import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
    dependencies {
        classpath("org.jooq:jooq-codegen:3.14.13")
    }
}

plugins {
    kotlin("jvm") version "1.6.10"
    id("com.diffplug.spotless") version "6.3.0"
    id("org.jetbrains.kotlin.plugin.spring") version "1.6.10"
    id("org.jetbrains.kotlin.plugin.jpa") version "1.6.10"
    id("org.springframework.boot") version "2.6.3"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    id("com.google.cloud.tools.jib") version "3.2.0"
    id("com.revolut.jooq-docker") version "0.3.7"
}

version = "1.1.0-SNAPSHOT"
java.targetCompatibility = JavaVersion.VERSION_11

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

repositories {
    jcenter()
    maven("https://jitpack.io")
}

spotless {
    kotlin {
        ktlint("0.44.0")
        this.targetExclude("build/**")
    }
    kotlinGradle {
        ktlint("0.44.0")
    }
}

tasks.withType<Test>().all {
    useJUnitPlatform()
}

jib {
    container {
        ports = listOf("8080")
        jvmFlags = listOf("-XX:ActiveProcessorCount=4")
    }
}

tasks["jib"].dependsOn(tasks["check"])

tasks.withType<com.revolut.jooq.GenerateJooqClassesTask>().all {
    basePackageName = "de.grannath.pardona.jooq"
    excludeFlywayTable = true
    customizeGenerator {
        withName("org.jooq.codegen.KotlinGenerator")
        generate.withPojos(true).withImmutablePojos(true)
    }
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect"))

    implementation("io.github.microutils:kotlin-logging:2.1.21")

    implementation("org.springframework.boot:spring-boot-starter-web")

    implementation("org.flywaydb:flyway-core")
    implementation("org.jooq:jooq")
    implementation("org.jooq:jooq-kotlin")
    jdbc("org.postgresql:postgresql")

    implementation("com.google.cloud:spring-cloud-gcp-starter-sql-postgresql:3.1.0")
    configurations["testImplementation"].exclude("com.google.cloud", "spring-cloud-gcp-starter-sql-postgresql")
    implementation("com.google.cloud:spring-cloud-gcp-starter-secretmanager:3.1.0")

    implementation("org.springframework.boot:spring-boot-starter-actuator")

    implementation("com.discord4j:discord4j-core:3.2.2")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.0")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor:1.6.0")

    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "junit")
    }
    testImplementation(group = "org.junit.jupiter", name = "junit-jupiter-api")
    testRuntimeOnly(group = "org.junit.jupiter", name = "junit-jupiter-engine")
    testImplementation("io.kotest:kotest-runner-junit5:5.1.0")
    testImplementation("io.kotest:kotest-framework-datatest:5.1.0")
    testImplementation("io.kotest.extensions:kotest-extensions-spring:1.1.0")
    testImplementation("io.kotest.extensions:kotest-extensions-testcontainers:1.2.1")
    testImplementation("org.testcontainers:postgresql:1.15.3")
    testImplementation("org.testcontainers:junit-jupiter:1.15.3")
    testRuntimeOnly("org.postgresql:postgresql")
}
