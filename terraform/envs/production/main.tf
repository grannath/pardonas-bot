module "db" {
  source = "../../modules/postgres"

  vpc_name      = "pardonas-bot"
  instance_name = "pardonas-bot-postgres"
  database_name = "pardonas-bot"
}
