package de.grannath.pardona.commands

import discord4j.common.util.Snowflake
import discord4j.core.GatewayDiscordClient
import discord4j.core.event.domain.interaction.ChatInputAutoCompleteEvent
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent
import discord4j.core.`object`.command.ApplicationCommandInteractionOption
import discord4j.core.`object`.command.ApplicationCommandOption
import discord4j.core.`object`.entity.Attachment
import discord4j.core.`object`.entity.User
import discord4j.core.spec.EmbedCreateSpec
import discord4j.core.spec.InteractionApplicationCommandCallbackSpec
import discord4j.discordjson.json.ApplicationCommandOptionChoiceData
import discord4j.discordjson.json.ApplicationCommandRequest
import kotlinx.coroutines.reactor.awaitSingleOrNull
import kotlinx.coroutines.reactor.mono
import mu.KotlinLogging
import org.springframework.context.annotation.Configuration
import reactor.core.publisher.Mono
import java.time.Duration
import javax.annotation.PostConstruct

@Configuration
class SlaschCommandRegistration(
    private val client: GatewayDiscordClient,
    private val commands: List<SlashCommand>,
) {
    private val logger = KotlinLogging.logger { }

    @PostConstruct
    fun registerCommands() {
        client.applicationInfo.cache().repeat()
            .zipWith(client.restClient.guilds)
            .map { it.t1 to it.t2 }
            .flatMap { (appInfo, guild) ->
                val service = client.restClient.applicationService
                service
                    .bulkOverwriteGuildApplicationCommand(
                        appInfo.id.asLong(),
                        guild.id().asLong(),
                        commands.map { it.command },
                    )
                    .doOnError { logger.error(it) { "Failed to update commands on guild ${guild.name()} (${guild.id()})." } }
                    .doOnNext { logger.info { "Updated command ${it.name()} (${it.id()}) on guild ${guild.name()} (${guild.id()})." } }
            }
            .doOnComplete { logger.info { "Command updates finished." } }
            .subscribe()
    }
}

class UserException(message: String) : RuntimeException(message)

abstract class SlashCommand(
    protected val client: GatewayDiscordClient,
) {
    private val logger = KotlinLogging.logger { }

    abstract val command: ApplicationCommandRequest

    init {
        client.on(ChatInputInteractionEvent::class.java)
            .filter { it.commandName == command.name() }
            .flatMap { event ->
                mono { handle(Invocation.of(event)) }
                    .timeout(Duration.ofSeconds(2))
                    .onErrorResume(UserException::class.java) {
                        event.reply(it.message!!).withEphemeral(true).map { }
                    }.onErrorResume {
                        logger.error(it) { "Error in command ${command.name()} (${command.description()})." }
                        event.reply("Da ist leider etwas schiefgegangen.").withEphemeral(true).map { }
                    }
            }
            .subscribe()
    }

    protected fun autocomplete(optionName: String, suggestionSupplier: suspend (User, String?) -> List<ApplicationCommandOptionChoiceData>) {
        client.on(ChatInputAutoCompleteEvent::class.java)
            .flatMap { event ->
                if (event.focusedOption.name == optionName) {
                    mono { suggestionSupplier(event.interaction.user, event.focusedOption.value.map { it.asString() }.orElse(null)) }
                        .flatMap { event.respondWithSuggestions(it) }
                        .onErrorResume {
                            logger.error(it) { "Autocomplete on option $optionName failed." }
                            event.respondWithSuggestions(emptyList())
                        }
                } else {
                    Mono.empty()
                }
            }
            .subscribe()
    }

    abstract suspend fun handle(event: Invocation)
}

sealed class Invocation(protected val event: ChatInputInteractionEvent) {
    abstract val name: String
    abstract val fullName: String
    val user: User = event.interaction.user
    val guildId: Snowflake? = event.interaction.guildId.orElse(null)
    val attachments: Map<Snowflake, Attachment> =
        event.interaction.commandInteraction.flatMap { it.resolved }.map { it.attachments }.orElse(emptyMap())

    abstract fun getStringOptionValue(name: String): String?

    abstract fun getIntOptionValue(name: String): Int?

    abstract fun getBooleanOptionValue(name: String): Boolean?

    abstract fun getUserOptionValue(name: String): Snowflake?

    suspend fun reply(embed: EmbedCreateSpec, ephemeral: Boolean = false): Unit =
        when (val recipient = getUserOptionValue(recipientOption.name())) {
            null -> {
                event.reply(
                    InteractionApplicationCommandCallbackSpec.create().withEmbeds(embed).withEphemeral(ephemeral)
                ).map { }.awaitSingleOrNull() ?: Unit
            }
            event.interaction.user.id -> {
                event.reply(
                    InteractionApplicationCommandCallbackSpec.create().withEmbeds(embed).withEphemeral(true)
                ).map { }.awaitSingleOrNull() ?: Unit
            }
            else -> {
                event.client.getUserById(recipient)
                    .map {
                        if (it.isBot) {
                            throw UserException("Ihr könnt leider keine Ergebnisse an Maschinen schicken.")
                        } else {
                            it
                        }
                    }
                    .flatMap { it.privateChannel }
                    .flatMap { it.createMessage(embed) }
                    .then(event.reply("Erledigt!").withEphemeral(true))
                    .map { }
                    .awaitSingleOrNull() ?: Unit
            }
        }

    class Command(
        event: ChatInputInteractionEvent,
    ) : Invocation(event) {
        override val name: String = event.commandName
        override val fullName: String = name

        override fun getStringOptionValue(name: String): String? =
            event.getOption(name).flatMap { it.value }.map { it.asString() }.orElse(null)

        override fun getIntOptionValue(name: String): Int? =
            event.getOption(name).flatMap { it.value }.map { it.asLong().toInt() }.orElse(null)

        override fun getBooleanOptionValue(name: String): Boolean? =
            event.getOption(name).flatMap { it.value }.map { it.asBoolean() }.orElse(null)

        override fun getUserOptionValue(name: String): Snowflake? =
            event.getOption(name).flatMap { it.value }.map { it.asSnowflake() }.orElse(null)
    }

    class Subcommand(
        event: ChatInputInteractionEvent,
        private val option: ApplicationCommandInteractionOption,
    ) : Invocation(event) {
        val parent: String = event.commandName
        override val name: String = option.name
        override val fullName: String = "$parent $name"

        override fun getStringOptionValue(name: String): String? =
            option.getOption(name).flatMap { it.value }.map { it.asString() }.orElse(null)

        override fun getIntOptionValue(name: String): Int? =
            option.getOption(name).flatMap { it.value }.map { it.asLong().toInt() }.orElse(null)

        override fun getBooleanOptionValue(name: String): Boolean? =
            option.getOption(name).flatMap { it.value }.map { it.asBoolean() }.orElse(null)

        override fun getUserOptionValue(name: String): Snowflake? =
            option.getOption(name).flatMap { it.value }.map { it.asSnowflake() }.orElse(null)
    }

    companion object {
        fun of(event: ChatInputInteractionEvent): Invocation =
            event.options.singleOrNull { it.type == ApplicationCommandOption.Type.SUB_COMMAND }
                ?.let { Subcommand(event, it) }
                ?: Command(event)
    }
}
