package de.grannath.pardona.commands

import de.grannath.pardona.jooq.keys.UNQ_RULE_SET
import de.grannath.pardona.jooq.tables.pojos.RuleSet
import de.grannath.pardona.jooq.tables.records.RuleSetRecord
import de.grannath.pardona.jooq.tables.references.RULE_SET
import discord4j.common.util.Snowflake
import discord4j.core.GatewayDiscordClient
import discord4j.core.`object`.command.ApplicationCommandOption
import discord4j.core.spec.EmbedCreateFields
import discord4j.core.spec.EmbedCreateSpec
import discord4j.discordjson.json.ApplicationCommandOptionData
import discord4j.discordjson.json.ApplicationCommandRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jooq.DSLContext
import org.jooq.impl.DSL.not
import org.springframework.stereotype.Component

@Component
class RuleCommands(
    private val create: DSLContext,
    client: GatewayDiscordClient,
) : SlashCommand(client) {
    override val command: ApplicationCommandRequest =
        ApplicationCommandRequest.builder()
            .name("regeln")
            .description("Schalte Regeln auf diesem Server ein und aus.")
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("aktuell")
                    .description("Zeige die aktuellen Regeln.")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("niedrige-lep")
                    .description("Sollen niedrige LeP/AuP die Eigenschaften beeinflussen?")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("eingeschaltet")
                            .description("Soll die Regel eingeschaltet sein?")
                            .type(ApplicationCommandOption.Type.BOOLEAN.value)
                            .required(true)
                            .build()
                    )
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("wunden")
                    .description("Sollen Wunden die Eigenschaften beeinflussen?")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("eingeschaltet")
                            .description("Soll die Regel eingeschaltet sein?")
                            .type(ApplicationCommandOption.Type.BOOLEAN.value)
                            .required(true)
                            .build()
                    )
                    .build()
            )
            .build()

    override suspend fun handle(event: Invocation) {
        if (event.guildId == null)
            throw UserException("Regeln können nur auf einem Server verwendet werden.")

        val response = when (event.name) {
            "aktuell" -> reportRules(event.guildId!!, "Dies sind die aktuellen Regeln auf diesem Server.")
            "niedrige-lep" -> create.setLep(
                event.guildId!!,
                event.getBooleanOptionValue("eingeschaltet")!!
            ).let { reportRules(event.guildId!!, "Die \"niedrige LeP\" Regel ist nun ${it.lep!!.enabled()}") }
            "wunden" -> create.setWunden(
                event.guildId!!,
                event.getBooleanOptionValue("eingeschaltet")!!
            ).let { reportRules(event.guildId!!, "Die \"Wunden\" Regel ist nun ${it.wunden!!.enabled()}") }
            else -> throw IllegalStateException("Unknown command ${event.fullName}.")
        }

        event.reply(response)
    }

    private suspend fun reportRules(guildId: Snowflake, message: String) =
        create.rules(guildId).let {
            EmbedCreateSpec.create()
                .withTitle("Regeln")
                .withDescription(message)
                .withFields(
                    EmbedCreateFields.Field.of(
                        "Niedrige LeP",
                        it.lep!!.enabled(),
                        true,
                    ),
                    EmbedCreateFields.Field.of(
                        "Wunden",
                        it.wunden!!.enabled(),
                        true,
                    ),
                )
        }
}

suspend fun DSLContext.rulesOrDefault(server: Snowflake?): RuleSet =
    if (server != null) {
        rules(server)
    } else {
        RuleSet(lep = true, wunden = true)
    }

suspend fun DSLContext.rules(server: Snowflake): RuleSet = withContext(Dispatchers.IO) {
    insertInto(RULE_SET)
        .set(server.newRuleSet())
        .onConflictOnConstraint(UNQ_RULE_SET)
        .doUpdate()
        .set(RULE_SET.SERVER, RULE_SET.SERVER)
        .returning()
        .fetchOne()!!
        .into(RuleSet::class.java)
}

private suspend fun DSLContext.setLep(server: Snowflake, enabled: Boolean): RuleSet = withContext(Dispatchers.IO) {
    insertInto(RULE_SET)
        .set(server.newRuleSet().withLep(enabled))
        .onConflictOnConstraint(UNQ_RULE_SET)
        .doUpdate()
        .set(RULE_SET.LEP, not(RULE_SET.LEP))
        .returning()
        .fetchOne()!!
        .into(RuleSet::class.java)
}

private suspend fun DSLContext.setWunden(server: Snowflake, enabled: Boolean): RuleSet = withContext(Dispatchers.IO) {
    insertInto(RULE_SET)
        .set(server.newRuleSet().withWunden(enabled))
        .onConflictOnConstraint(UNQ_RULE_SET)
        .doUpdate()
        .set(RULE_SET.WUNDEN, not(RULE_SET.WUNDEN))
        .returning()
        .fetchOne()!!
        .into(RuleSet::class.java)
}

private fun Snowflake.newRuleSet() =
    RuleSetRecord().apply {
        server = this@newRuleSet.asString()
        lep = true
        wunden = true
    }

private fun RuleSetRecord.withLep(enabled: Boolean) = apply {
    lep = enabled
}

private fun RuleSetRecord.withWunden(enabled: Boolean) = apply {
    wunden = enabled
}

private fun Boolean.enabled() =
    when (this) {
        true -> "eingeschaltet"
        false -> "ausgeschaltet"
    }
