package de.grannath.pardona

import mu.KotlinLogging
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.util.UriComponentsBuilder

@Controller
class AddBotController(properties: DiscordProperties) {
    private val logger = KotlinLogging.logger { }

    private val uri = UriComponentsBuilder.newInstance()
        .scheme("https")
        .host("discord.com")
        .path("api/oauth2/authorize")
        .queryParam("client_id", properties.clientId)
        .queryParam("permissions", "2048")
        .queryParam("scope", "guilds.members.read bot applications.commands")
        .build()
        .toUri().also {
            logger.info { "Bot authorization URL: $it" }
        }

    @GetMapping("/addbot")
    fun addBot() =
        ResponseEntity.status(302)
            .location(uri)
            .body("Redirecting to discord servers...")
}
