terraform {
  backend "gcs" {
    bucket = "pardonas-bot-terraform"
    prefix = "state"
  }
}
