package de.grannath.pardona.commands

import discord4j.core.`object`.command.ApplicationCommandOption
import discord4j.discordjson.json.ApplicationCommandOptionData

val recipientOption =
    ApplicationCommandOptionData.builder()
        .name("empfaenger")
        .description("Wähle einen Empfänger für das Ergebnis")
        .type(ApplicationCommandOption.Type.USER.value)
        .required(false)
        .build()
