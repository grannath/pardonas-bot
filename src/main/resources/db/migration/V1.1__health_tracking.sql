CREATE OR REPLACE FUNCTION "both_null_or_at_most"("current" INT, "max" INT)
    RETURNS BOOLEAN
AS
'SELECT "current" IS NULL AND "max" IS NULL OR "current" IS NOT NULL AND "max" IS NOT NULL AND "current" <= "max"'
    LANGUAGE "sql";

CREATE TABLE "character_status"
(
    "id"          BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    "character_id" bigint  NOT NULL,
    "max_lep"     INT NOT NULL CHECK ( "max_lep" > 0 ),
    "current_lep" INT NOT NULL CHECK ( "current_lep" <= "max_lep" ),
    "max_aup"     INT NOT NULL CHECK ( "max_aup" > 0 ),
    "current_aup" INT NOT NULL CHECK ( "current_aup" <= "max_aup" ),
    "max_asp"     INT CHECK ( "max_asp" > 0 ),
    "current_asp" INT CHECK ( both_null_or_at_most("current_asp", "max_asp") ),
    "max_kap"     INT CHECK ( "max_kap" > 0 ),
    "current_kap" INT CHECK ( both_null_or_at_most("current_kap", "max_kap") )
);

ALTER TABLE "character_status"
    ADD CONSTRAINT "FK_status_character"
        FOREIGN KEY ("character_id") REFERENCES "character" ("id");

ALTER TABLE "character_status"
    ADD CONSTRAINT "UNQ_character_status"
        UNIQUE ("character_id");
