package de.grannath.pardona.commands

import de.grannath.pardona.characters.selectCharacter
import de.grannath.pardona.jooq.tables.references.CHARACTER
import de.grannath.pardona.jooq.tables.references.INVENTORY
import discord4j.common.util.Snowflake
import discord4j.core.GatewayDiscordClient
import discord4j.core.`object`.command.ApplicationCommandOption
import discord4j.core.`object`.entity.User
import discord4j.core.spec.EmbedCreateFields
import discord4j.core.spec.EmbedCreateSpec
import discord4j.discordjson.json.ApplicationCommandOptionData
import discord4j.discordjson.json.ApplicationCommandRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jooq.DSLContext
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.StringJoiner

@Component
class InventoryCommand(
    private val create: DSLContext,
    client: GatewayDiscordClient,
) : SlashCommand(client) {
    override val command: ApplicationCommandRequest =
        ApplicationCommandRequest.builder()
            .name("inventar")
            .description("Pflege das Inventar deines Charakters.")
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("geld")
                    .description("Nimm Geld aus deinem Beutel oder lege welches hinein.")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(characterOption)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("dukaten")
                            .description("Anzahl der Dukaten")
                            .type(ApplicationCommandOption.Type.INTEGER.value)
                            .required(false)
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("silbertaler")
                            .description("Anzahl der Silbertaler")
                            .type(ApplicationCommandOption.Type.INTEGER.value)
                            .required(false)
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("heller")
                            .description("Anzahl der Heller")
                            .type(ApplicationCommandOption.Type.INTEGER.value)
                            .required(false)
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("kreuzer")
                            .description("Anzahl der Kreuzer")
                            .type(ApplicationCommandOption.Type.INTEGER.value)
                            .required(false)
                            .build()
                    )
                    .build()
            )
            .build()

    override suspend fun handle(event: Invocation) {
        val response = when (event.name) {
            "geld" -> updateMoney(
                event.user,
                event.characterName,
                event.getIntOptionValue("dukaten") ?: 0,
                event.getIntOptionValue("silbertaler") ?: 0,
                event.getIntOptionValue("heller") ?: 0,
                event.getIntOptionValue("kreuzer") ?: 0,
            )
            else -> throw IllegalStateException("Unknown command ${event.fullName}.")
        }.withTimestamp(Instant.now())
            .withFooter(EmbedCreateFields.Footer.of("Pardona's Bot", null))

        event.reply(response, ephemeral = true)
    }

    private suspend fun updateMoney(
        user: User,
        characterName: String?,
        dukaten: Int,
        silber: Int,
        heller: Int,
        kreuzer: Int
    ): EmbedCreateSpec {
        val moneyDiff = Money(
            dukaten * 1000L +
                silber * 100L +
                heller * 10L +
                kreuzer
        )

        if (moneyDiff == ZERO)
            return reportInventory(user, characterName)

        val (characterId, oldAmount) = create.loadMoney(user.id, characterName)

        val newAmount = oldAmount + moneyDiff

        return if (newAmount < ZERO) {
            throw UserException("${user.mention}, so viel Geld besitzt ihr gar nicht!")
        } else {
            create.setMoney(characterId, newAmount.amount)
            EmbedCreateSpec.create()
                .withTitle("Geldbörse")
                .withDescription(
                    if (moneyDiff >= ZERO) {
                        "${user.mention}, ihr habt ${moneyDiff.format()} erhalten. Insgesamt besitzt ihr nun:"
                    } else {
                        "${user.mention}, ihr habt ${(-moneyDiff).format()} ausgegeben. Nun besitzt ihr noch:"
                    }
                )
                .withFields(
                    EmbedCreateFields.Field.of(
                        "Vermögen",
                        newAmount.format(),
                        false,
                    )
                )
        }
    }

    private suspend fun reportInventory(user: User, characterName: String?): EmbedCreateSpec {
        val (_, money) = create.loadMoney(user.id, characterName)

        return EmbedCreateSpec.create()
            .withTitle("Geldbörse")
            .withDescription("${user.mention}, ihr besitzt zur Zeit folgendes:")
            .withFields(
                EmbedCreateFields.Field.of(
                    "Vermögen",
                    money.format(),
                    false,
                )
            )
    }
}

@JvmInline
value class Money(val amount: Long) : Comparable<Money> {
    override fun compareTo(other: Money): Int =
        this.amount.compareTo(other.amount)
}

val ZERO = Money(0)

operator fun Money.unaryMinus() =
    Money(-this.amount)

operator fun Money.plus(other: Money) =
    Money(this.amount + other.amount)

operator fun Money.times(factor: Long) =
    Money(this.amount * factor)

operator fun Money.times(factor: Int) =
    Money(this.amount * factor)

private fun Long.nullIfZero() =
    if (this == 0L) null else this

private fun Money.format(): String {
    val dukaten = (amount / 1000).nullIfZero()?.let { "$it Dukaten" }
    val silber = ((amount % 1000) / 100).nullIfZero()?.let { "$it Silber" }
    val heller = ((amount % 100) / 10).nullIfZero()?.let { "$it Heller" }
    val kreuzer = (amount % 10).nullIfZero()?.let { "$it Kreuzer" }
    return StringJoiner(", ")
        .addIfNotNull(dukaten)
        .addIfNotNull(silber)
        .addIfNotNull(heller)
        .addIfNotNull(kreuzer)
        .setEmptyValue("0 Kreuzer")
        .toString()
}

private suspend fun DSLContext.loadMoney(userId: Snowflake, characterName: String?) = withContext(Dispatchers.IO) {
    select(INVENTORY.CHARACTER_ID, INVENTORY.MONEY)
        .from(INVENTORY)
        .join(CHARACTER)
        .on(INVENTORY.CHARACTER_ID.eq(CHARACTER.ID))
        .selectCharacter(userId, characterName)
        .fetchOne { it.get(INVENTORY.CHARACTER_ID)!! to Money(it.get(INVENTORY.MONEY)!!) }!!
}

private suspend fun DSLContext.setMoney(characterId: Long, amount: Long) = withContext(Dispatchers.IO) {
    update(INVENTORY)
        .set(INVENTORY.MONEY, amount)
        .where(INVENTORY.CHARACTER_ID.eq(characterId))
        .execute()
}
