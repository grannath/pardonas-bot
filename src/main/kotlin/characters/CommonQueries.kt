package de.grannath.pardona.characters

import de.grannath.pardona.jooq.keys.UNQ_CHARACTER_STATUS
import de.grannath.pardona.jooq.tables.pojos.EigenschaftValue
import de.grannath.pardona.jooq.tables.records.EigenschaftValueRecord
import de.grannath.pardona.jooq.tables.records.TalentValueRecord
import de.grannath.pardona.jooq.tables.records.ZauberValueRecord
import de.grannath.pardona.jooq.tables.references.CHARACTER
import de.grannath.pardona.jooq.tables.references.CHARACTER_STATUS
import de.grannath.pardona.jooq.tables.references.EIGENSCHAFT_VALUE
import de.grannath.pardona.jooq.tables.references.TALENT_VALUE
import de.grannath.pardona.jooq.tables.references.USER
import de.grannath.pardona.jooq.tables.references.ZAUBER_VALUE
import discord4j.common.util.Snowflake
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.jooq.Configuration
import org.jooq.DSLContext
import org.jooq.Field
import org.jooq.Record
import org.jooq.ResultQuery
import org.jooq.SelectJoinStep
import org.jooq.TableLike
import org.jooq.TransactionalCallable

fun <R : Record> SelectJoinStep<R>.selectCharacter(userId: Snowflake, character: String?) =
    if (character == null) {
        join(USER)
            .on(USER.SELECTED_CHARACTER_ID.eq(CHARACTER.ID))
            .where(USER.DISCORD_ID.eq(userId.asString()))
    } else {
        join(USER)
            .on(CHARACTER.USER_ID.eq(USER.ID))
            .where(CHARACTER.NAME.eq(character))
            .and(USER.DISCORD_ID.eq(userId.asString()))
    }

suspend fun DSLContext.characterExists(discordId: Snowflake, characterName: String?) = withContext(Dispatchers.IO) {
    selectOne()
        .from(CHARACTER)
        .selectCharacter(discordId, characterName)
        .fetchAny() != null
}

suspend fun DSLContext.fetchEigenschaften(discordId: Snowflake, characterName: String?): List<CharacterEigenschaft> = withContext(Dispatchers.IO) {
    select(EIGENSCHAFT_VALUE.asterisk())
        .from(EIGENSCHAFT_VALUE)
        .join(CHARACTER)
        .on(EIGENSCHAFT_VALUE.CHARACTER_ID.eq(CHARACTER.ID))
        .selectCharacter(discordId, characterName)
        .fetchInto<EigenschaftValue>()
        .map {
            CharacterEigenschaft(
                it.name!!,
                it.value!!,
            )
        }
}

suspend fun DSLContext.updateCharacter(characterId: Long, characterDocument: String) = withContext(Dispatchers.IO) {
    update(CHARACTER)
        .set(CHARACTER.DOCUMENT, characterDocument)
        .where(CHARACTER.ID.eq(characterId))
        .returning()
        .fetchOne()!!
        .into<Character>()
}

suspend fun DSLContext.replaceEigenschaften(characterId: Long, eigenschaften: List<CharacterEigenschaft>) = withContext(Dispatchers.IO) {
    transaction { configuration: Configuration ->
        configuration.dsl().deleteFrom(EIGENSCHAFT_VALUE)
            .where(EIGENSCHAFT_VALUE.CHARACTER_ID.eq(characterId))
            .execute()
        configuration.dsl().batchStore(
            eigenschaften.map {
                EigenschaftValueRecord()
                    .with(EIGENSCHAFT_VALUE.NAME, it.name)
                    .with(EIGENSCHAFT_VALUE.VALUE, it.value)
                    .with(EIGENSCHAFT_VALUE.CHARACTER_ID, characterId)
            }
        )
            .execute()
    }
}

suspend fun DSLContext.replaceTalente(characterId: Long, talente: List<CharacterTalent>) = withContext(Dispatchers.IO) {
    transaction { configuration ->
        configuration.dsl().deleteFrom(TALENT_VALUE)
            .where(TALENT_VALUE.CHARACTER_ID.eq(characterId))
            .execute()
        configuration.dsl().batchInsert(
            talente.map {
                TalentValueRecord()
                    .with(TALENT_VALUE.NAME, it.name)
                    .with(TALENT_VALUE.VALUE, it.value)
                    .with(TALENT_VALUE.CHECK_STRING, it.checkString)
                    .with(TALENT_VALUE.CHARACTER_ID, characterId)
            }
        )
            .execute()
    }
}

suspend fun DSLContext.replaceZauber(characterId: Long, zauber: List<CharacterZauber>) = withContext(Dispatchers.IO) {
    transaction { configuration: Configuration ->
        configuration.dsl().deleteFrom(ZAUBER_VALUE)
            .where(ZAUBER_VALUE.CHARACTER_ID.eq(characterId))
            .execute()
        configuration.dsl().batchStore(
            zauber.map {
                ZauberValueRecord()
                    .with(ZAUBER_VALUE.NAME, it.name)
                    .with(ZAUBER_VALUE.VALUE, it.value)
                    .with(ZAUBER_VALUE.CHECK_STRING, it.checkString)
                    .with(ZAUBER_VALUE.CHARACTER_ID, characterId)
            }
        )
            .execute()
    }
}

private val CheckValue.checkString: String
    get() = "(${check.first}/${check.second}/${check.third})"

suspend fun DSLContext.updateStatusFromDocument(characterId: Long, characterStatus: CharacterStatus) = withContext(Dispatchers.IO) {
    insertInto(CHARACTER_STATUS)
        .set(CHARACTER_STATUS.CHARACTER_ID, characterId)
        .set(CHARACTER_STATUS.MAX_LEP, characterStatus.maxLep)
        .set(CHARACTER_STATUS.CURRENT_LEP, characterStatus.maxLep)
        .set(CHARACTER_STATUS.MAX_AUP, characterStatus.maxAup)
        .set(CHARACTER_STATUS.CURRENT_AUP, characterStatus.maxAup)
        .set(CHARACTER_STATUS.MAX_ASP, characterStatus.maxAsp)
        .set(CHARACTER_STATUS.CURRENT_ASP, characterStatus.maxAsp)
        .set(CHARACTER_STATUS.MAX_KAP, characterStatus.maxKap)
        .set(CHARACTER_STATUS.CURRENT_KAP, characterStatus.maxKap)
        .onConflictOnConstraint(UNQ_CHARACTER_STATUS)
        .doUpdate()
        .set(CHARACTER_STATUS.MAX_LEP, characterStatus.maxLep)
        .set(CHARACTER_STATUS.MAX_AUP, characterStatus.maxAup)
        .set(CHARACTER_STATUS.MAX_ASP, characterStatus.maxAsp)
        .set(CHARACTER_STATUS.MAX_KAP, characterStatus.maxKap)
        .where(CHARACTER_STATUS.CHARACTER_ID.eq(characterId))
        .execute()
}

suspend fun <T> DSLContext.transaction(code: suspend CoroutineScope.(DSLContext) -> T) = withContext(Dispatchers.IO) {
    transactionResult(
        TransactionalCallable {
            runBlocking {
                code(it.dsl())
            }
        }
    )
}

inline fun <reified T> ResultQuery<*>.fetchInto() =
    fetchInto(T::class.java)

inline fun <reified T> ResultQuery<*>.fetchOneInto() =
    fetchOneInto(T::class.java)

inline fun <reified T> Record.into(): T =
    into(T::class.java)

inline fun <reified T> TableLike<*>.field(name: String): Field<T>? =
    field(name, T::class.java)
