package de.grannath.pardona

import discord4j.core.DiscordClientBuilder
import discord4j.core.GatewayDiscordClient
import mu.KotlinLogging
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableConfigurationProperties(DiscordProperties::class)
class BotConfiguration {
    private val logger = KotlinLogging.logger {}

    @Bean
    fun discordClient(properties: DiscordProperties): GatewayDiscordClient {
        with(properties.token) {
            logger.info {
                "Logging in at discord with token ${replaceRange(5, length, "*".repeat(length - 5))}."
            }
        }

        return DiscordClientBuilder.create(properties.token)
            .build().login().block()!!
    }
}

@ConfigurationProperties("discord")
@ConstructorBinding
class DiscordProperties(
    val token: String,
    val clientId: String,
    val permissions: Int,
)
