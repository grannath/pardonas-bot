# pardonas-bot
Pardona's Bot - Das Schwarze Auge in Discord!

Dieser Bot erlaubt DSA Proben in Discord.
Möglich sind Eigenschaftsproben, Talentproben und Schadenswürfe mit Modifikatoren.
Proben können offen oder geheim geworfen werden.
Werte können direkt aus der Heldensoftware importiert werden.


## Befehle
Alle Befehle können in einem beliebigen Kanal gesendet werden oder als direct message.
Die Befehle folgen dem folgenden Muster.

 - !**name** _arg1_ _arg2_ ... \[opt. arg1\] \[opt. arg2\] ...

### Charakter hochladen
Um einen Character aus der Heldensoftware zu importieren, muss dieser zunächst dort exportiert werden.
Dazu beim Export unbedingt das _XML_ Format auswählen.

Dann kann bei Discord die XML Datei hochgeladen werden, zusammen mit der Nachricht

 - !**ichbin** \[Name\]

Der Name ist optional, es wird sonst der Name aus dem Heldendokument verwendet.

Es können mehrere Helden hochgeladen werden.
Habt ihr einmal einen Charakter hinzugefügt, könnt ihr ihn jederzeit mit

 - !**ichbin** _Name_

wieder auswählen.
Um den Helden zu aktualisieren, einfach die neue Version des Dokuments mit dem selben Namen hochladen.

### Charaktere managen
Der aktuelle Charakter und alle anderen können jederzeit überprüft werden mit

 - !**werbinich**

Ein hochgeladener Character kann auch gelöscht werden.
Das passiert mit dem Befehl

 - !**vergiss** _Name_

### Regeln und Mali

Die Effekte von Wunden und niedrigen LeP können automatisch bei Proben einberechnet werden.
Dies kann pro Server ein- und ausgestellt werden.

Die aktuellen Regeln können mit

 - !**regeln**

abgefragt werden.
Zum Ändern wird einfach

 - !**regeln** _Regelname_

als Befehl eingegeben.
Die bisher implementierten Regeln sind `wunden` und `lep`.

### Charakterstatus
Lebenspunkte, Ausdauerpunkte, etc. können ebenfalls genutzt werden.
Die aktuellen Werte werden durch

 - !**status**

ausgegeben.

Für Änderungen gibt es einzelne Kommandos **lep**, **sp**, **aup** **spa**, **asp**, **kap**.
Alle folgen dem Muster

 - !**lep** _Würfelausdruck_

Wie Würfelausdrücke aussehen, wird weiter unten beschrieben.
Je nachdem ob Schaden oder Heilung eingetragen werden soll, müssen die Zahlen entsprechend negativ oder positiv sein.
Zur Abkürzung gibt es auch SP und SPA als Befehl, einfacher Lebenspunkte und Ausdauer abziehen zu können.
"lep -10" ist also dasselbe wie "sp 10".

Auch Wunden können hier eingetragen werden.

 - !**wunden** _Anzahl_ _Zone_

Die Anzahl kann positiv oder negativ sein, je nachdem ob Wunden erhalten werden oder verheilen.

### Eigenschaftsproben
Es kann eine Probe gegen einen Eigenschaftswert geworfen werden (oder Attacke oder eine andere Probe mit einem w20).
Die Erschwernis kann optional als zweites Argument angegeben werden.

 - !**e** _Eigenschaftswert_ \[Erschwernis\]
 - !**eigenschaft** _Eigenschaftswert_ \[Erschwernis\]

Der _Eigenschaftswert_ ist entweder eine Zahl oder der Name bzw. die Abkürzung für eine der 8 Basiseigenschaften.


### Talentproben
Es kann eine Talentprobe geworfen werden.
Der Talentwert kann als erstes optionales Argument angegeben werden.
Die Erschwernis kann als zweites optionales Argument angegeben werden.
Nur eine Erschwernis ohne Talentwert ist nicht möglich.
Ist kein Wert angegeben, wird ein TaW von 0 angenommen.

 - !**t** _Eigenschaftswert1_ _Eigenschaftswert2_ _Eigenschaftswert3_ \[Talentwert\] \[Erschwernis\]
 - !**talent** _Eigenschaftswert1_ _Eigenschaftswert2_ _Eigenschaftswert3_ \[Talentwert\] \[Erschwernis\]

Der _Eigenschaftswert_ ist entweder eine Zahl oder der Name bzw. die Abkürzung für eine der 8 Basiseigenschaften.
Der _Talentwert_ kann ebenfalls als Zahl angegeben werden oder als Name eines Talents.
Der Name muss nur soweit ausgeschrieben werden, dass er eindeutig ist (z.B. "sinnen" für "Sinnenschärfe").

Statt alle Eigenschaften auszuschreiben, kann auch ein Talent per Name direkt gewählt werden.

 - !**t** _Talentname_ \[Erschwernis\]
 - !**talent** _Talentname_ \[Erschwernis\]

### Inventar
Das Inventar besteht bisher nur aus Geld.
Das aktuelle Inventar wird angezeigt mit

 - !**inv**

Es kann Geld hinzugefügt und herausgenommen werden.
Das passiert durch

 - !**inv** \[+\] _Menge_ _Münze_ \[Menge Münze \[...\]\]
 - !**inv** _-_ _Menge_ _Münze_ \[Menge Münze \[...\]\]

Bekannte Münzen sind Dukaten, Silbertaler, Heller und Kreuzer.
Es reicht schon der Anfangsbuchstabe einer der vier Münzen.

### Generelle Würfelwürfe
Es können auch beliebige Würfel geworfen werden.
Die Ergebnisse können addiert und subtrahiert werden.

 - !**r** _Würfelausdruck_
 - !**roll** _Würfelausdruck_

Ein Würfelausdruck besteht aus Würfelangaben in der Form _"nwx"_ oder _"ndx"_, wobei _n_ die Anzahl der Würfel ist und _x_ die Anzahl der Seiten der Würfel.
Fehlt _n_, wird 1 angenommen.
Ebenfalls sind fixe Zahlen möglich.
Solche Ausdrücke können mit _+_ oder _-_ verbunden werden.
Leerzeichen können benutzt werden, sind aber optional.

Beispiele:
 - 2w6
 - d3 + 2
 - 2w6-1
 - 1w20 + 2d14
 - 3-w3

## Modifikatoren
Bevor einem Befehl können Modifikatoren angegeben werden.
Diese beeinflussen, wie das Ergebnis zurückgegeben wird.

### Versteckte Proben
Das Ergebnis einer Probe kann einem Empfänger als direct message mitgeteilt werden.

 - !**h** _Empfänger_ _Befehl_
 - !**hidden** _Empfänger_ _Befehl_

### Proben mit einem anderen Charakter
Wenn ihr einen Helden ausgewählt habt, aber eine einzelne Probe mit einem anderen werfen müsst, könnt ihr das mit einem Modifikator.

 - !**c** _Name_ _Befehl_
 - !**character** _Name_ _Befehl_
