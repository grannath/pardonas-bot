package de.grannath.pardona.commands.talents

import de.grannath.pardona.checks.CheckInput
import de.grannath.pardona.checks.EigenschaftInput
import de.grannath.pardona.checks.TalentCheckResult
import de.grannath.pardona.checks.TalentCheckResult.CriticalFailure
import de.grannath.pardona.checks.TalentCheckResult.CriticalSuccess
import de.grannath.pardona.checks.TalentCheckResult.EpicSuccess
import de.grannath.pardona.checks.TalentCheckResult.Failure
import de.grannath.pardona.checks.TalentCheckResult.Success
import de.grannath.pardona.commands.COLOR_CRIT_FAILURE
import de.grannath.pardona.commands.COLOR_CRIT_SUCCESS
import de.grannath.pardona.commands.COLOR_FAILURE
import de.grannath.pardona.commands.COLOR_SUCCESS
import de.grannath.pardona.commands.Invocation
import de.grannath.pardona.commands.signedString
import discord4j.core.spec.EmbedCreateFields
import discord4j.core.spec.EmbedCreateSpec
import java.time.Instant

fun TalentCheckResult.toMessage(invocation: Invocation): EmbedCreateSpec =
    EmbedCreateSpec.create()
        .withTitle("Talentprobe")
        .withDescription("${invocation.user.mention}, ${personalMessage()}")
        .withTimestamp(Instant.now())
        .withColor(color())
        .withFields(detailsFields(invocation, this))
        .withFooter(EmbedCreateFields.Footer.of("Pardona's Bot", null))

private fun TalentCheckResult.color() =
    when (this) {
        is CriticalFailure -> COLOR_CRIT_FAILURE
        is CriticalSuccess -> COLOR_CRIT_SUCCESS
        is EpicSuccess -> COLOR_CRIT_SUCCESS
        is Failure -> COLOR_FAILURE
        is Success -> COLOR_SUCCESS
    }

private fun detailsFields(invocation: Invocation, result: TalentCheckResult): List<EmbedCreateFields.Field> =
    if (invocation.name == "standard") {
        listOf(
            standardEigenschaftenField(result.input),
            talentField(result.input),
            modifierField(result.input),
        )
    } else {
        listOf(
            freieEigenschaftenField(result.input),
            talentField(result.input),
            modifierField(result.input),
        )
    } + resultFields(result)

private fun standardEigenschaftenField(input: CheckInput): EmbedCreateFields.Field =
    when (input) {
        is CheckInput.Named ->
            EmbedCreateFields.Field.of(
                input.eigenschaften.let { "${it.first.labelWithState}/${it.second.labelWithState}/${it.third.labelWithState}" },
                input.eigenschaften.let { "${it.first.effectiveValue}/${it.second.effectiveValue}/${it.third.effectiveValue}" },
                true,
            )
        is CheckInput.Number ->
            throw IllegalArgumentException("Standard Eigenschaften field can only be created with named Talent.")
    }

private val EigenschaftInput.labelWithState: String
    get() = when (this) {
        is EigenschaftInput.Named -> if (stateModifier != 0) "${eigenschaft.shortForm}${stateModifier.signedString()}" else eigenschaft.shortForm
        is EigenschaftInput.Number -> throw IllegalArgumentException("No label with state for raw Eigenschaft inputs.")
    }

private fun freieEigenschaftenField(input: CheckInput): EmbedCreateFields.Field =
    EmbedCreateFields.Field.of(
        "Eigenschaften",
        "${input.eigenschaften.first.valueString}/${input.eigenschaften.second.valueString}/${input.eigenschaften.third.valueString}",
        true,
    )

private val EigenschaftInput.valueString: String
    get() = when (this) {
        is EigenschaftInput.Named ->
            "$labelWithState ($effectiveValue)"
        is EigenschaftInput.Number ->
            effectiveValue.toString()
    }

private fun talentField(input: CheckInput): EmbedCreateFields.Field =
    when (input) {
        is CheckInput.Named ->
            EmbedCreateFields.Field.of(
                input.talent.name,
                input.rawValue.toString(),
                true,
            )
        is CheckInput.Number ->
            EmbedCreateFields.Field.of(
                "Talent",
                input.rawValue.toString(),
                true,
            )
    }

private fun modifierField(input: CheckInput): EmbedCreateFields.Field =
    EmbedCreateFields.Field.of(
        "Modifikator",
        when (input) {
            is CheckInput.Named -> input.userModifier
            is CheckInput.Number -> input.modifier
        }.toString(),
        true,
    )

private fun resultFields(result: TalentCheckResult): List<EmbedCreateFields.Field> =
    when (result) {
        is CriticalFailure ->
            listOf(
                EmbedCreateFields.Field.of("Würfe", "${result.firstRoll}/${result.secondRoll}/${result.thirdRoll}", true),
            )
        is CriticalSuccess ->
            listOf(
                EmbedCreateFields.Field.of("Würfe", "${result.firstRoll}/${result.secondRoll}/${result.thirdRoll}", true),
                EmbedCreateFields.Field.of("TaP*", result.tapStar.toString(), true),
            )
        is Failure ->
            listOf(
                EmbedCreateFields.Field.of("Würfe", "${result.firstRoll}/${result.secondRoll}/${result.thirdRoll}", true),
                EmbedCreateFields.Field.of("max. Erschwernis", result.maxModifier.toString(), true),
            )
        is Success ->
            listOf(
                EmbedCreateFields.Field.of("Würfe", "${result.firstRoll}/${result.secondRoll}/${result.thirdRoll}", true),
                EmbedCreateFields.Field.of("TaP*", result.tapStar.toString(), true),
                EmbedCreateFields.Field.of("max. zusätzl. Erschwernis", result.maxAddModifier.toString(), true),
            )
        is EpicSuccess ->
            listOf(
                EmbedCreateFields.Field.of("Würfe", "1/1/1", true),
                EmbedCreateFields.Field.of("TaP*", result.tapStar.toString(), true),
            )
    }

private fun TalentCheckResult.personalMessage() =
    when (this) {
        is CriticalFailure ->
            "Was für ein schreckliches Schicksal, Sterbliche(r)!"
        is CriticalSuccess ->
            "Unglaublich! Das ein(e) Sterbliche(r) zu solchen Leistungen im Stande ist."
        is EpicSuccess ->
            "Ich verneige mich vor euch. Ich seid kein(e) gewöhnliche(r) Sterbliche(r)."
        is Failure ->
            "Das war wohl nichts. Viel Glück beim nächsten Mal."
        is Success ->
            "Ein Erfolg, für eine(n) Sterbliche(n)."
    }
