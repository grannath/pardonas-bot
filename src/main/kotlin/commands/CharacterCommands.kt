package de.grannath.pardona.commands

import de.grannath.pardona.characters.Character
import de.grannath.pardona.characters.User
import de.grannath.pardona.characters.getCharacterStatus
import de.grannath.pardona.characters.getEigenschaften
import de.grannath.pardona.characters.getName
import de.grannath.pardona.characters.getTalente
import de.grannath.pardona.characters.getZauber
import de.grannath.pardona.characters.into
import de.grannath.pardona.characters.parseDocument
import de.grannath.pardona.characters.replaceEigenschaften
import de.grannath.pardona.characters.replaceTalente
import de.grannath.pardona.characters.replaceZauber
import de.grannath.pardona.characters.selectCharacter
import de.grannath.pardona.characters.serialize
import de.grannath.pardona.characters.transaction
import de.grannath.pardona.characters.updateCharacter
import de.grannath.pardona.characters.updateStatusFromDocument
import de.grannath.pardona.jooq.tables.references.CHARACTER
import de.grannath.pardona.jooq.tables.references.CHARACTER_STATUS
import de.grannath.pardona.jooq.tables.references.INVENTORY
import de.grannath.pardona.jooq.tables.references.USER
import de.grannath.pardona.jooq.tables.references.WUNDE
import discord4j.common.util.Snowflake
import discord4j.core.GatewayDiscordClient
import discord4j.core.`object`.command.ApplicationCommandOption
import discord4j.core.spec.EmbedCreateFields
import discord4j.core.spec.EmbedCreateSpec
import discord4j.discordjson.json.ApplicationCommandOptionData
import discord4j.discordjson.json.ApplicationCommandRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.withContext
import org.jooq.DSLContext
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Component
import org.springframework.web.client.exchange
import java.time.Instant

@Component
class CharacterCommands(
    private val create: DSLContext,
    private val restTemplateBuilder: RestTemplateBuilder,
    client: GatewayDiscordClient,
) : SlashCommand(client) {
    override val command: ApplicationCommandRequest =
        ApplicationCommandRequest.builder()
            .name("character")
            .description("Wähle deinen Charakter.")
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("aktuell")
                    .description("Liste deine aktuellen Charaktere.")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("update")
                    .description("Lade einen Charakter hoch.")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("dokument")
                            .description("Das XML Dokument des Charakters aus der Heldensoftware")
                            .type(ApplicationCommandOption.Type.ATTACHMENT.value)
                            .required(true)
                            .build()
                    )
                    .addOption(characterOption)
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("waehlen")
                    .description("Wähle deinen aktuellen Charakter.")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .from(characterOption)
                            .required(true)
                            .build()
                    )
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("loeschen")
                    .description("Lösche einen Charakter.")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .from(characterOption)
                            .required(true)
                            .build()
                    )
                    .build()
            )
            .build()

    override suspend fun handle(event: Invocation) {
        val response = when (event.name) {
            "aktuell" -> listCharacters(event)
            "update" -> createOrUpdateCharacter(event)
            "waehlen" -> selectCharacter(event.user, event.characterName!!)
            "loeschen" -> deleteCharacter(event.user, event.characterName!!)
            else -> throw IllegalStateException("Unknown subcommand ${event.name}.")
        }.withTimestamp(Instant.now())
            .withFooter(EmbedCreateFields.Footer.of("Pardona's Bot", null))

        event.reply(response, ephemeral = true)
    }

    private suspend fun listCharacters(invocation: Invocation): EmbedCreateSpec {
        val selected = create.fetchSelectedCharacterName(invocation.user.id, invocation.characterName)
        val all = create.fetchAllCharacterNames(invocation.user.id)

        return if (all.isEmpty()) {
            EmbedCreateSpec.create()
                .withTitle("Charaktere")
                .withDescription("${invocation.user.mention}, ich kenne euch leider noch nicht.")
        } else if (selected != null) {
            EmbedCreateSpec.create()
                .withTitle("Charaktere")
                .withDescription("${invocation.user.mention}, ich kenne euch als $selected.")
                .withFields(
                    EmbedCreateFields.Field.of(
                        "Alle Charaktere",
                        all.joinToString("\n") { "- $it" },
                        false,
                    )
                )
        } else {
            EmbedCreateSpec.create()
                .withTitle("Charaktere")
                .withDescription("${invocation.user.mention}, sagt mir welchen eurer Namen Ihr bevorzugt..")
                .withFields(
                    EmbedCreateFields.Field.of(
                        "Alle Charaktere",
                        all.joinToString("\n") { "- $it" },
                        false,
                    )
                )
        }
    }

    private suspend fun createOrUpdateCharacter(invocation: Invocation): EmbedCreateSpec {
        val attachmentUrl = invocation.attachments.values.single().url
        val providedName = invocation.characterName
        val name = create.updateCharacterFromDocument(
            invocation.user.id,
            providedName,
            restTemplateBuilder.getDocument(attachmentUrl)
        ).name!!

        return selectCharacter(invocation.user, name)
    }

    private suspend fun selectCharacter(user: discord4j.core.`object`.entity.User, characterName: String): EmbedCreateSpec {
        create.selectCharacter(user.id, characterName)
        return EmbedCreateSpec.create()
            .withTitle("Aktueller Charakter")
            .withDescription("${user.mention}, Ihr seid von nun an bekannt als $characterName.")
    }

    private suspend fun RestTemplateBuilder.getDocument(url: String) =
        withContext(Dispatchers.IO) {
            this@getDocument.build()
                .exchange<String>(
                    url,
                    HttpMethod.GET,
                    HttpEntity<Any>(
                        HttpHeaders().apply { this.set("User-Agent", "PardonasBot") }
                    )
                )
                .body!!
        }

    private suspend fun deleteCharacter(user: discord4j.core.`object`.entity.User, characterName: String): EmbedCreateSpec =
        create.deleteCharacter(user.id, characterName)
            .let {
                EmbedCreateSpec.create()
                    .withTitle("Vergessener Charakter")
                    .withDescription("${user.mention}, ich habe $it vergessen. Seine/Ihre Seele hat die dritte Sphäre verlassen.")
            }
}

private suspend fun DSLContext.fetchSelectedCharacterName(discordId: Snowflake, characterName: String?) = withContext(Dispatchers.IO) {
    select(CHARACTER.NAME)
        .from(CHARACTER)
        .selectCharacter(discordId, characterName)
        .fetchOne(0) as String?
}

private suspend fun DSLContext.fetchAllCharacterNames(discordId: Snowflake): List<String> = withContext(Dispatchers.IO) {
    select(CHARACTER.NAME)
        .from(CHARACTER)
        .join(USER)
        .on(CHARACTER.USER_ID.eq(USER.ID))
        .where(USER.DISCORD_ID.eq(discordId.asString()))
        .fetch(CHARACTER.NAME)
        .filterNotNull()
}

private suspend fun DSLContext.updateCharacterFromDocument(discordId: Snowflake, characterName: String?, document: String): Character = withContext(Dispatchers.IO) {
    val doc = parseDocument(document)
    val name = characterName ?: doc.getName()
    val id = select(CHARACTER.ID)
        .from(CHARACTER)
        .selectCharacter(discordId, characterName)
        .fetchOne(CHARACTER.ID)

    transaction { dsl: DSLContext ->
        val user = async { dsl.fetchUser(discordId) ?: dsl.insertUser(discordId) }

        val newChar = if (id != null)
            dsl.updateCharacter(id, doc.serialize())
        else
            dsl.insertCharacter(user.await().id!!, name, doc.serialize())

        val e = async { dsl.replaceEigenschaften(newChar.id!!, doc.getEigenschaften()) }
        val t = async { dsl.replaceTalente(newChar.id!!, doc.getTalente()) }
        val z = async { dsl.replaceZauber(newChar.id!!, doc.getZauber()) }
        val s = async { dsl.updateStatusFromDocument(newChar.id!!, doc.getCharacterStatus()) }
        val i = async {
            withContext(Dispatchers.IO) {
                dsl.insertInto(INVENTORY, INVENTORY.CHARACTER_ID)
                    .values(newChar.id)
                    .onConflictDoNothing()
                    .execute()
            }
        }
        awaitAll(e, t, z, s, i)
        newChar
    }
}

private suspend fun DSLContext.fetchUser(discordId: Snowflake) = withContext(Dispatchers.IO) {
    selectFrom(USER)
        .where(USER.DISCORD_ID.eq(discordId.asString()))
        .fetchOneInto(User::class.java)
}

private suspend fun DSLContext.insertUser(discordId: Snowflake) = withContext(Dispatchers.IO) {
    insertInto(USER)
        .set(USER.DISCORD_ID, discordId.asString())
        .setNull(USER.SELECTED_CHARACTER_ID)
        .returning()
        .fetchOne()!!
        .into<User>()
}

private suspend fun DSLContext.unselectCharacter(discordId: String) = withContext(Dispatchers.IO) {
    update(USER)
        .setNull(USER.SELECTED_CHARACTER_ID)
        .where(USER.DISCORD_ID.eq(discordId))
        .execute()
}

private suspend fun DSLContext.deleteCharacterStatus(characterId: Long) = withContext(Dispatchers.IO) {
    deleteFrom(CHARACTER_STATUS)
        .where(CHARACTER_STATUS.CHARACTER_ID.eq(characterId))
        .execute()
}

private suspend fun DSLContext.deleteCharacterWunden(characterId: Long) = withContext(Dispatchers.IO) {
    deleteFrom(WUNDE)
        .where(WUNDE.CHARACTER_ID.eq(characterId))
        .execute()
}

private suspend fun DSLContext.deleteCharacterInventory(characterId: Long) = withContext(Dispatchers.IO) {
    deleteFrom(INVENTORY)
        .where(INVENTORY.CHARACTER_ID.eq(characterId))
        .execute()
}

private suspend fun DSLContext.selectCharacter(discordId: Snowflake, characterName: String) = withContext(Dispatchers.IO) {
    val id = select(CHARACTER.ID)
        .from(CHARACTER)
        .selectCharacter(discordId, characterName)
        .fetchOne(CHARACTER.ID)

    if (id == null)
        throw UserException("Ich kenne euch noch nicht. Bitte, zeigt mir Euren Heldenbrief, damit wir uns bekannt machen können.")
    else {
        update(USER)
            .set(USER.SELECTED_CHARACTER_ID, id)
            .where(USER.DISCORD_ID.eq(discordId.asString()))
            .execute()
        characterName
    }
}

private suspend fun DSLContext.insertCharacter(userId: Long, characterName: String, characterDocument: String) = withContext(Dispatchers.IO) {
    insertInto(CHARACTER)
        .set(CHARACTER.NAME, characterName)
        .set(CHARACTER.DOCUMENT, characterDocument)
        .set(CHARACTER.USER_ID, userId)
        .returning()
        .fetchOne()!!
        .into<Character>()
}

private suspend fun DSLContext.deleteCharacter(characterId: Long) = withContext(Dispatchers.IO) {
    deleteFrom(CHARACTER)
        .where(CHARACTER.ID.eq(characterId))
        .execute()
}

private suspend fun DSLContext.deleteCharacter(discordId: Snowflake, characterName: String): String = withContext(Dispatchers.IO) {
    val id = select(CHARACTER.ID)
        .from(CHARACTER)
        .selectCharacter(discordId, characterName)
        .fetchOne(CHARACTER.ID)
        ?: throw UserException("Ich kenne niemanden names $characterName")

    transaction { dsl: DSLContext ->
        val e = async { dsl.replaceEigenschaften(id, listOf()) }
        val t = async { dsl.replaceTalente(id, listOf()) }
        val z = async { dsl.replaceZauber(id, listOf()) }
        val s = async { dsl.unselectCharacter(discordId.asString()) }
        val st = async { dsl.deleteCharacterStatus(id) }
        val w = async { dsl.deleteCharacterWunden(id) }
        val i = async { dsl.deleteCharacterInventory(id) }
        awaitAll(e, t, z, s, st, w, i)

        dsl.deleteCharacter(id)
        characterName
    }
}
