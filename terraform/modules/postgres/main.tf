resource "google_compute_network" "vpc" {
  name = var.vpc_name
}

output "vpc_id" {
  value = google_compute_network.vpc.id
}

resource "google_compute_firewall" "vpc_ssh" {
  name          = "${var.vpc_name}-allow-ssh"
  network       = google_compute_network.vpc.name
  source_ranges = ["0.0.0.0/0"]
  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
}

resource "google_compute_firewall" "vpc_http" {
  name          = "${var.vpc_name}-allow-http"
  network       = google_compute_network.vpc.name
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["http-server"]
  allow {
    protocol = "tcp"
    ports    = ["80"]
  }
}

resource "google_compute_firewall" "vpc_https" {
  name          = "${var.vpc_name}-allow-https"
  network       = google_compute_network.vpc.name
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["https-server"]
  allow {
    protocol = "tcp"
    ports    = ["443"]
  }
}

resource "google_compute_firewall" "vpc_internal" {
  name          = "${var.vpc_name}-allow-internal"
  network       = google_compute_network.vpc.name
  source_ranges = ["10.128.0.0/9"]
  allow {
    protocol = "tcp"
    ports    = ["0-65535"]
  }
  allow {
    protocol = "udp"
    ports    = ["0-65535"]
  }
  allow {
    protocol = "icmp"
  }
}

resource "google_compute_global_address" "private_ip_address" {
  name          = "${google_compute_network.vpc.name}-private-ip-address"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = google_compute_network.vpc.id
}

resource "google_service_networking_connection" "private_vpc_connection" {
  network                 = google_compute_network.vpc.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}

resource "google_sql_database_instance" "master" {
  name             = var.instance_name
  database_version = "POSTGRES_11"

  settings {
    # Second-generation instance tiers are based on the machine
    # type. See argument reference below.
    tier              = "db-f1-micro"
    availability_type = "ZONAL"

    ip_configuration {
      ipv4_enabled    = false
      private_network = google_compute_network.vpc.id
      require_ssl     = false
    }

    database_flags {
      name  = "cloudsql.iam_authentication"
      value = "on"
    }
    database_flags {
      name  = "max_connections"
      value = "50"
    }
    maintenance_window {
      day  = 1
      hour = 2
    }
  }
}

output "instance_path" {
  value = "${google_sql_database_instance.master.project}:${google_sql_database_instance.master.region}:${google_sql_database_instance.master.name}"
}

resource "google_sql_database" "database" {
  name     = var.database_name
  instance = google_sql_database_instance.master.name
}

output "database_name" {
  value = google_sql_database.database.name
}
