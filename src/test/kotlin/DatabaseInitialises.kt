package de.grannath.pardona

import io.kotest.core.spec.style.StringSpec
import io.kotest.extensions.spring.SpringExtension
import io.kotest.extensions.testcontainers.perTest
import org.springframework.boot.test.autoconfigure.jooq.JooqTest
import org.springframework.boot.test.util.TestPropertyValues
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.test.context.ContextConfiguration
import org.testcontainers.containers.PostgreSQLContainer

@ContextConfiguration(initializers = [DatabaseInitialises.Companion.DbInitializer::class])
@JooqTest
class DatabaseInitialises : StringSpec() {

    init {
        listener(postgreSQLContainer.perTest())
        "The application context starts and initialises the database" {
            // nothing to check here
        }
    }

    override fun extensions() = listOf(SpringExtension)

    companion object {
        val postgreSQLContainer = PostgreSQLContainer<Nothing>("postgres:11.1")

        class DbInitializer : ApplicationContextInitializer<ConfigurableApplicationContext> {
            override fun initialize(configurableApplicationContext: ConfigurableApplicationContext) {
                postgreSQLContainer.start()
                TestPropertyValues
                    .of(
                        "spring.datasource.driver-class-name=" + postgreSQLContainer.driverClassName,
                        "spring.datasource.url=" + postgreSQLContainer.jdbcUrl,
                        "spring.datasource.username=" + postgreSQLContainer.username,
                        "spring.datasource.password=" + postgreSQLContainer.password
                    ).applyTo(configurableApplicationContext.environment)
            }
        }
    }
}
