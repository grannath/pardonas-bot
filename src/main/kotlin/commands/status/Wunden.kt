package de.grannath.pardona.commands.status

import de.grannath.pardona.characters.Wunde
import de.grannath.pardona.characters.characterExists
import de.grannath.pardona.characters.field
import de.grannath.pardona.characters.into
import de.grannath.pardona.characters.selectCharacter
import de.grannath.pardona.commands.UserException
import de.grannath.pardona.jooq.enums.WundenLocation
import de.grannath.pardona.jooq.tables.references.CHARACTER
import de.grannath.pardona.jooq.tables.references.WUNDE
import discord4j.common.util.Snowflake
import discord4j.core.`object`.entity.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jooq.DSLContext
import org.jooq.impl.DSL

suspend fun updateWunden(user: User, characterName: String?, wundenLocation: WundenLocation, count: Int, create: DSLContext): String {
    if (!create.characterExists(user.id, characterName))
        throw UserException("Ich kenne euch doch gar nicht! Dann kann ich euch auch nicht verletzen.")

    @Suppress("UNCHECKED_CAST")
    return when {
        count > 0 -> {
            repeat(count) {
                create.addWunde(
                    discordId = user.id,
                    characterName = characterName,
                    location = wundenLocation
                )
            }
            "Ihr habt $count Wunden ${wundenLocation.asAdverb} erhalten."
        }

        count < 0 -> {
            val trueCount = create.removeWunden(
                discordId = user.id,
                characterName = characterName,
                location = wundenLocation,
                count = -count
            )
            "$trueCount eurer Wunden ${wundenLocation.asAdverb} haben sich geschlossen."
        }

        else -> "Ich habe also nichts getan, wie ihr es verlangt habt."
    }
}

private val WundenLocation.asAdverb: String
    get() = when (this) {
        WundenLocation.kopf -> "am Kopf"
        WundenLocation.brust -> "an der Brust"
        WundenLocation.arm_links -> "am linken Arm"
        WundenLocation.arm_rechts -> "am rechten Arm"
        WundenLocation.bauch -> "am Bauch"
        WundenLocation.bein_links -> "am linken Bein"
        WundenLocation.bein_rechts -> "am rechten Bein"
    }

private suspend fun DSLContext.addWunde(
    discordId: Snowflake,
    characterName: String?,
    location: WundenLocation
): Wunde = withContext(Dispatchers.IO) {
    val selectedCharacter = DSL.name("selected_character")
        .fields("id")
        .`as`(select(CHARACTER.ID).from(CHARACTER).selectCharacter(discordId, characterName))
    with(selectedCharacter)
        .insertInto(WUNDE)
        .set(WUNDE.LOCATION, location)
        .set(WUNDE.CHARACTER_ID, select(selectedCharacter.field<Long>("id")).from(selectedCharacter))
        .returning()
        .fetchOne()!!
        .into<Wunde>()
}

private suspend fun DSLContext.removeWunden(
    discordId: Snowflake,
    characterName: String?,
    location: WundenLocation,
    count: Int
): Int = withContext(Dispatchers.IO) {
    val oldestWunde = DSL.name("oldest_wunde")
        .fields("id")
        .`as`(
            select(WUNDE.ID)
                .from(WUNDE)
                .join(CHARACTER)
                .on(WUNDE.CHARACTER_ID.eq(CHARACTER.ID))
                .selectCharacter(discordId, characterName)
                .and(WUNDE.LOCATION.eq(location))
                .orderBy(WUNDE.RECEIVED_AT.asc())
                .limit(count)
        )
    with(oldestWunde)
        .delete(WUNDE)
        .where(WUNDE.ID.`in`(select(oldestWunde.field<Long>("id")).from(oldestWunde)))
        .execute()
}
