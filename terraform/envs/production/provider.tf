provider "google" {
  project = "pardonas-bot"
  region  = "europe-west1"
  zone    = "europe-west1-b"
}
