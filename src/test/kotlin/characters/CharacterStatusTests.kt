package de.grannath.pardona.characters

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class CharacterStatusTests : StringSpec({
    suspend fun zahir() =
        parseDocument(javaClass.getResourceAsStream("/ZahirSagittaroSgirra.xml").readAllBytes().decodeToString())

    suspend fun meneander() =
        parseDocument(javaClass.getResourceAsStream("/MeneanderPydoctis.xml").readAllBytes().decodeToString())

    "should calculate max LeP correctly" {
        zahir().getCharacterStatus().maxLep shouldBe 35
        meneander().getCharacterStatus().maxLep shouldBe 30
    }

    "should calculate max AuP correctly" {
        zahir().getCharacterStatus().maxAup shouldBe 38
        meneander().getCharacterStatus().maxAup shouldBe 34
    }

    "should calculate max KaP correctly" {
        zahir().getCharacterStatus().maxKap shouldBe 27
        meneander().getCharacterStatus().maxKap shouldBe null
    }

    "should calculate max AsP correctly" {
        zahir().getCharacterStatus().maxAsp shouldBe null
        meneander().getCharacterStatus().maxAsp shouldBe 49
    }
})
