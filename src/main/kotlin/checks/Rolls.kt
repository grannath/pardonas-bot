package de.grannath.pardona.checks

import de.grannath.pardona.commands.UserException
import java.security.SecureRandom
import java.util.Locale

typealias RollSource = (Int) -> Int

internal val random = SecureRandom()

val defaultRollSource: RollSource = { random.nextInt(it) + 1 }

fun roll(sides: Int, source: RollSource = defaultRollSource): Int =
    if (sides < 1) {
        throw UserException("Ein Würfel muss wenigstens eine Seite haben, nicht $sides.")
    } else {
        source(sides)
    }

fun rollExpression(expression: String, source: RollSource = defaultRollSource): Int =
    with(Token.parseString(expression.lowercase(Locale.GERMAN)).iterator()) {
        var result = when (val init = next()) {
            is ConstantToken -> init.value
            is RollToken -> init.roll(source)
            is OperationToken -> init.operation.operation(0, next().value(source))
        }

        while (hasNext()) {
            result = next().apply(result, next().value(source))
        }

        return result
    }

private sealed class Token {
    companion object {
        fun parseString(string: String): List<Token> =
            string.split(splitPattern).map(String::trim).filter { it.isNotBlank() }.map(::fromString)

        private fun fromString(string: String) =
            when {
                string.matches(operatorPattern) -> OperationToken(Operations.fromChar(string.single()))

                string.matches(constantPattern) -> ConstantToken(string.toInt())

                string.matches(expressionPattern) -> with(string.split(rollSplitPattern)) {
                    RollToken(this[0].toInt(), this[1].toInt())
                }

                else -> throw UserException("$string ist kein Würfelausdruck!")
            }
    }
}

private data class RollToken(val rolls: Int, val sides: Int) : Token()

private data class ConstantToken(val value: Int) : Token()

private data class OperationToken(val operation: Operations) : Token()

private enum class Operations(val char: Char, val operation: (Int, Int) -> Int) {
    PLUS('+', Int::plus),
    MINUS('-', Int::minus);

    companion object {
        fun fromChar(char: Char) =
            values().first { it.char == char }
    }
}

private val splitPattern = Regex("((?<=[+-])|(?=[+-]))")
private val operatorPattern = Regex("[+-]")
private val expressionPattern = Regex("(\\d*)[wdWD](\\d+)")
private val rollSplitPattern = Regex("[wdWD]")
private val constantPattern = Regex("(\\d+)")

private fun RollToken.roll(source: RollSource) =
    (1..rolls).fold(0) { res, _ ->
        res + roll(sides, source)
    }

private fun Token.value(source: RollSource) =
    when (this) {
        is RollToken -> roll(source)
        is ConstantToken -> value
        is OperationToken -> throw IllegalStateException("Expected a value, not an operation!")
    }

private fun Token.apply(first: Int, second: Int) =
    when (this) {
        is RollToken -> throw IllegalStateException("Expected an operation, not a roll!")
        is ConstantToken -> throw IllegalStateException("Expected an operation, not a value!")
        is OperationToken -> operation.operation(first, second)
    }
