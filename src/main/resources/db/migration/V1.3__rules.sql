CREATE TABLE "rule_set"
(
    "id"     BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    "server" VARCHAR NOT NULL,
    "lep"    BOOLEAN NOT NULL,
    "wunden" BOOLEAN NOT NULL
);

ALTER TABLE "rule_set"
    ADD CONSTRAINT "UNQ_rule_set"
        UNIQUE ("server");
