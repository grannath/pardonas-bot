package de.grannath.pardona.commands

import de.grannath.pardona.checks.rollExpression
import discord4j.core.GatewayDiscordClient
import discord4j.core.`object`.command.ApplicationCommandOption
import discord4j.core.spec.EmbedCreateFields
import discord4j.core.spec.EmbedCreateSpec
import discord4j.discordjson.json.ApplicationCommandOptionData
import discord4j.discordjson.json.ApplicationCommandRequest
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class RollCommand(
    client: GatewayDiscordClient,
) : SlashCommand(client) {
    override val command: ApplicationCommandRequest =
        ApplicationCommandRequest.builder()
            .name("roll")
            .description("Wirf eine Hand voll Würfel!")
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("wuerfel")
                    .description("Die Art und Anzahl der Würfel (z.B. \"3w6 + 2\").")
                    .type(ApplicationCommandOption.Type.STRING.value)
                    .required(true)
                    .build()
            )
            .addOption(recipientOption)
            .build()

    override suspend fun handle(event: Invocation) {
        val expression = event.getStringOptionValue("wuerfel")!!
        val result = rollExpression(expression)

        event.reply(
            EmbedCreateSpec.create()
                .withTitle("Würfelwurf")
                .withDescription("${event.user.mention}, ich habe für Euch die Würfel geworfen.")
                .withFields(
                    EmbedCreateFields.Field.of(
                        "Würfel",
                        expression,
                        true,
                    ),
                    EmbedCreateFields.Field.of(
                        "Ergebnis",
                        result.toString(),
                        true,
                    ),
                )
                .withTimestamp(Instant.now())
                .withFooter(EmbedCreateFields.Footer.of("Pardona's Bot", null))
        )
    }
}
