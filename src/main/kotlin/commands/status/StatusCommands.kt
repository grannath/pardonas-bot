package de.grannath.pardona.commands.status

import de.grannath.pardona.characters.CharacterStatus
import de.grannath.pardona.characters.State
import de.grannath.pardona.characters.Wunde
import de.grannath.pardona.characters.and
import de.grannath.pardona.characters.andEigenschaften
import de.grannath.pardona.characters.andTalente
import de.grannath.pardona.characters.andZauber
import de.grannath.pardona.characters.asString
import de.grannath.pardona.characters.fetchInto
import de.grannath.pardona.characters.fetchOneInto
import de.grannath.pardona.characters.field
import de.grannath.pardona.characters.plus
import de.grannath.pardona.characters.selectCharacter
import de.grannath.pardona.commands.Invocation
import de.grannath.pardona.commands.SlashCommand
import de.grannath.pardona.commands.UserException
import de.grannath.pardona.commands.characterName
import de.grannath.pardona.commands.characterOption
import de.grannath.pardona.commands.rulesOrDefault
import de.grannath.pardona.jooq.enums.WundenLocation
import de.grannath.pardona.jooq.tables.references.CHARACTER
import de.grannath.pardona.jooq.tables.references.CHARACTER_STATUS
import de.grannath.pardona.jooq.tables.references.WUNDE
import discord4j.common.util.Snowflake
import discord4j.core.GatewayDiscordClient
import discord4j.core.`object`.command.ApplicationCommandOption
import discord4j.core.spec.EmbedCreateFields
import discord4j.core.spec.EmbedCreateSpec
import discord4j.discordjson.json.ApplicationCommandOptionChoiceData
import discord4j.discordjson.json.ApplicationCommandOptionData
import discord4j.discordjson.json.ApplicationCommandRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext
import org.jooq.DSLContext
import org.jooq.Record1
import org.jooq.SelectConditionStep
import org.jooq.impl.DSL
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class StatusCommands(
    private val create: DSLContext,
    client: GatewayDiscordClient,
) : SlashCommand(client) {
    override val command: ApplicationCommandRequest =
        ApplicationCommandRequest.builder()
            .name("status")
            .description("Schau dir den Zustand deines Charakters an.")
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("aktuell")
                    .description("Der aktuelle Status (LeP, AsP, ...) deines Charakters.")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(characterOption)
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("vollheilung")
                    .description("Alle Wunden werden geheilt, Lebenspunkte, Astralenergie, etc. werden komplett aufgefüllt.")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(characterOption)
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("lep")
                    .description("Aktualisiere die Lebenspunkte deines Charakters.")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("lep")
                            .description("Die Differenz der LeP (auch als Würfel, z.B. \"1w6\")")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .required(true)
                            .build()
                    )
                    .addOption(characterOption)
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("sp")
                    .description("Füge Schadenspunkte deinem Charakter zu.")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("sp")
                            .description("Die zuzufügenden Schadenspunkte (auch als Würfel, z.B. \"1w6\")")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .required(true)
                            .build()
                    )
                    .addOption(characterOption)
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("aup")
                    .description("Aktualisiere die Ausdauer deines Charakters.")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("aup")
                            .description("Die Differenz der AuP (auch als Würfel, z.B. \"1w6\")")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .required(true)
                            .build()
                    )
                    .addOption(characterOption)
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("asp")
                    .description("Aktualisiere die Astralenergie deines Charakters.")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("asp")
                            .description("Die Differenz der AsP (auch als Würfel, z.B. \"1w6\")")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .required(true)
                            .build()
                    )
                    .addOption(characterOption)
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("kap")
                    .description("Aktualisiere die Karmalenergie deines Charakters.")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("kap")
                            .description("Die Differenz der KaP (auch als Würfel, z.B. \"1w6\")")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .required(true)
                            .build()
                    )
                    .addOption(characterOption)
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("wunden")
                    .description("Aktualisiere die Wunden deines Charakters.")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("zone")
                            .description("Die betroffene Trefferzone")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .choices(WundenLocation.values().map { ApplicationCommandOptionChoiceData.builder().name(it.asString()).value(it.toString()).build() })
                            .required(true)
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("anzahl")
                            .description("Die Differenz in der Anzahl der Wunden")
                            .type(ApplicationCommandOption.Type.INTEGER.value)
                            .required(true)
                            .build()
                    )
                    .addOption(characterOption)
                    .build()
            )
            .build()

    override suspend fun handle(event: Invocation) {
        val message = when (event.name) {
            "aktuell" -> currentStatusMessage(event)
            "vollheilung" -> resetStatus(event)
            "lep" -> updateLep(
                invocation = event,
                expression = event.getStringOptionValue("lep")!!,
                subtract = false,
                create = create
            )
            "sp" -> updateLep(
                invocation = event,
                expression = event.getStringOptionValue("sp")!!,
                subtract = true,
                create = create
            )
            "aup" -> updateAup(
                invocation = event,
                expression = event.getStringOptionValue("aup")!!,
                subtract = true,
                create = create
            )
            "asp" -> updateAsp(
                invocation = event,
                expression = event.getStringOptionValue("asp")!!,
                create = create
            )
            "kap" -> updateKap(
                invocation = event,
                expression = event.getStringOptionValue("kap")!!,
                create = create
            )
            "wunden" -> updateWunden(
                user = event.user,
                characterName = event.characterName,
                wundenLocation = WundenLocation.valueOf(event.getStringOptionValue("zone")!!),
                count = event.getIntOptionValue("anzahl")!!,
                create = create
            )
            else -> throw IllegalStateException("Unknown command ${event.fullName}.")
        }

        event.reply(
            EmbedCreateSpec.create()
                .withTitle("Euer Zustand")
                .withDescription(message)
                .withFields(
                    statusFields(invocation = event)
                )
                .withTimestamp(Instant.now())
                .withFooter(EmbedCreateFields.Footer.of("Pardona's Bot", null)),
            ephemeral = true,
        )
    }

    private suspend fun statusFields(invocation: Invocation) =
        currentStatus(invocation, create).let { status ->
            listOfNotNull(
                EmbedCreateFields.Field.of(
                    "LeP",
                    "${status.currentLep}/${status.maxLep}",
                    true,
                ),
                EmbedCreateFields.Field.of(
                    "AuP",
                    "${status.currentAup}/${status.maxAup}",
                    true,
                ),
                if (status.maxAsp != null) {
                    EmbedCreateFields.Field.of(
                        "AsP",
                        "${status.currentAsp}/${status.maxAsp}",
                        true,
                    )
                } else {
                    null
                },
                if (status.maxKap != null) {
                    EmbedCreateFields.Field.of(
                        "KaP",
                        "${status.currentKap}/${status.maxKap}",
                        true,
                    )
                } else {
                    null
                },
                when (val wunden = currentStates(invocation.guildId, invocation.user.id, invocation.characterName, create)) {
                    emptyList<Any>() -> null
                    else ->
                        EmbedCreateFields.Field.of(
                            "Wunden",
                            wunden.joinToString("\n") { "- ${it.asString()}" },
                            false,
                        )
                }
            )
        }

    private suspend fun currentStatusMessage(invocation: Invocation): String = coroutineScope {
        val states = currentStates(
            serverId = invocation.guildId,
            discordId = invocation.user.id,
            characterName = invocation.characterName,
            create = create
        )

        when {
            states.flatMap { it.modifiers }.any { it.name == "sterbend" } -> "ihr seid so gut wie tot."
            states.flatMap { it.modifiers }.any { it.name == "kampfunfähig" || it.name == "bewusstlos" } -> "ihr könnt euch kaum noch bewegen."
            states.any { it.name.contains("3 Wunden") || it.name == "LeP unter 1/4" } -> "ihr seht sehr schwer verletzt aus."
            states.any { it.name.contains("2 Wunde(n)") || it.name == "LeP unter 1/3" } -> "ihr seht schwer verletzt aus."
            states.any { it.name.contains("1 Wunde(n)") || it.name == "LeP unter 1/2" } -> "ihr seht verletzt aus."
            else -> "ihr seht recht gesund aus."
        }
    }

    private suspend fun resetStatus(invocation: Invocation): String {
        val status = currentStatus(invocation, create)

        withContext(Dispatchers.IO) {
            create.update(CHARACTER_STATUS)
                .set(CHARACTER_STATUS.CURRENT_LEP, status.maxLep)
                .set(CHARACTER_STATUS.CURRENT_AUP, status.maxAup)
                .set(CHARACTER_STATUS.CURRENT_ASP, status.maxAsp)
                .set(CHARACTER_STATUS.CURRENT_KAP, status.maxKap)
                .where(CHARACTER_STATUS.ID.eq(status.id))
                .execute()

            @Suppress("UNCHECKED_CAST")
            val allWunden = DSL.name("wunden")
                .fields("id")
                .`as`(
                    create.select(WUNDE.ID)
                        .from(WUNDE)
                        .join(CHARACTER)
                        .on(WUNDE.CHARACTER_ID.eq(CHARACTER.ID))
                        .selectCharacter(invocation.user.id, invocation.characterName) as SelectConditionStep<Record1<Long>>
                )
            create.with(allWunden)
                .delete(WUNDE)
                .where(WUNDE.ID.`in`(create.select(allWunden.field<Long>("id")).from(allWunden)))
                .execute()
        }

        return "${invocation.user.mention}, ihr seid nun vollständig geheilt."
    }
}

suspend fun currentState(invocation: Invocation, create: DSLContext) =
    currentStates(invocation.guildId, invocation.user.id, invocation.characterName, create)
        .reduceOrNull(State::plus) ?: State(name = "Gesamt")

private suspend fun currentStates(
    serverId: Snowflake?,
    discordId: Snowflake,
    characterName: String?,
    create: DSLContext,
) = coroutineScope {
    val asyncStatus = async { create.fetchCharacterStatus(discordId, characterName) }

    val wunden = async { create.fetchWunden(discordId, characterName) }

    val ruleSet = async { create.rulesOrDefault(serverId) }

    val status = asyncStatus.await()
    val lepStates = if (ruleSet.await().lep!!) {
        listOfNotNull(status.lepState(), status.aupState())
    } else emptyList()
    val wundenStates = if (ruleSet.await().wunden!!) {
        wunden.await().states()
    } else emptyList()

    lepStates + wundenStates
}

suspend fun currentStatus(invocation: Invocation, create: DSLContext) =
    create.fetchCharacterStatus(invocation.user.id, invocation.characterName)
        ?: if (invocation.characterName == null) {
            throw UserException("Ich kenne euch leider nicht, bitte stellt euch zuerst vor.")
        } else {
            throw UserException("Ich kenne niemanden namens \"${invocation.characterName}\".")
        }

private suspend fun DSLContext.fetchCharacterStatus(
    discordId: Snowflake,
    characterName: String?,
): CharacterStatus? = withContext(Dispatchers.IO) {
    select(CHARACTER_STATUS.asterisk())
        .from(CHARACTER_STATUS)
        .join(CHARACTER)
        .on(CHARACTER_STATUS.CHARACTER_ID.eq(CHARACTER.ID))
        .selectCharacter(discordId, characterName)
        .fetchOneInto<CharacterStatus>()
}

private suspend fun DSLContext.fetchWunden(discordId: Snowflake, characterName: String?): List<Wunde> = withContext(Dispatchers.IO) {
    select(WUNDE.asterisk())
        .from(WUNDE)
        .join(CHARACTER)
        .on(WUNDE.CHARACTER_ID.eq(CHARACTER.ID))
        .selectCharacter(discordId, characterName)
        .fetchInto<Wunde>()
}

private fun CharacterStatus?.lepState(): State? =
    when {
        this == null -> null
        this.currentLep!! <= 0 -> State("LeP auf/unter 0").and("sterbend")
        this.currentLep <= 5 -> State("LeP auf/unter 5").and("kampfunfähig")
        this.currentLep <= this.maxLep!! / 4 -> State("LeP unter 1/4").lepLevel(3)
        this.currentLep <= this.maxLep / 3 -> State("LeP unter 1/3").lepLevel(2)
        this.currentLep <= this.maxLep / 2 -> State("LeP unter 1/2").lepLevel(1)
        else -> null
    }

private fun CharacterStatus?.aupState(): State? =
    when {
        this == null -> null
        this.currentAup!! <= 0 -> State("AuP auf 0").and("handlungsunfähig")
        this.currentAup <= this.maxAup!! / 4 -> State("AuP unter 1/4").lepLevel(2)
        this.currentAup <= this.maxAup / 3 -> State("AuP unter 1/3").lepLevel(1)
        else -> null
    }

private fun State.lepLevel(level: Int) =
    this.andEigenschaften(-1 * level)
        .andTalente(-3 * level)
        .andZauber(-3 * level)
        .and("gs", -1 * level)

private fun List<Wunde>.states(): List<State> =
    groupBy { it.location }
        .map { (location, wunden) ->
            when (location!!) {
                WundenLocation.kopf -> if (wunden.size >= 3)
                    State("3 Wunden am Kopf")
                        .and("bewusstlos")
                        .and("Blutverlust")
                else
                    State("${wunden.size} Wunde(n) am Kopf")
                        .and("mu", -2 * wunden.size)
                        .and("kl", -2 * wunden.size)
                        .and("in", -2 * wunden.size)
                        .and("ini", -2 * wunden.size)
                WundenLocation.brust -> if (wunden.size >= 3)
                    State("3 Wunden an der Brust")
                        .and("bewusstlos")
                        .and("Blutverlust")
                else
                    State("${wunden.size} Wunde(n) an der Brust")
                        .and("at", -1 * wunden.size)
                        .and("pa", -1 * wunden.size)
                        .and("ko", -1 * wunden.size)
                        .and("kk", -1 * wunden.size)
                WundenLocation.arm_rechts -> if (wunden.size >= 3)
                    State("3 Wunden am rechten Arm")
                        .and("Rechter Arm handlungsunfähig")
                else
                    State("${wunden.size} Wunde(n) am rechten Arm")
                        .and("at", -2 * wunden.size)
                        .and("pa", -2 * wunden.size)
                        .and("kk", -2 * wunden.size)
                        .and("ff", -2 * wunden.size)
                WundenLocation.arm_links -> if (wunden.size >= 3)
                    State("3 Wunden am linken Arm")
                        .and("Linker Arm handlungsunfähig")
                else
                    State("${wunden.size} Wunde(n) am linken Arm")
                        .and("at", -2 * wunden.size)
                        .and("pa", -2 * wunden.size)
                        .and("kk", -2 * wunden.size)
                        .and("ff", -2 * wunden.size)
                WundenLocation.bauch -> if (wunden.size >= 3)
                    State("3 Wunden am Bauch")
                        .and("bewusstlos")
                        .and("Blutverlust")
                else
                    State("${wunden.size} Wunde(n) am Bauch")
                        .and("at", -1 * wunden.size)
                        .and("pa", -1 * wunden.size)
                        .and("ko", -1 * wunden.size)
                        .and("kk", -1 * wunden.size)
                        .and("gs", -1 * wunden.size)
                        .and("ini", -1 * wunden.size)
                WundenLocation.bein_links -> if (wunden.size >= 3)
                    State("3 Wunden am linken Bein")
                        .and("kampfunfähig")
                else
                    State("${wunden.size} Wunde(n) am linken Bein")
                        .and("at", -2 * wunden.size)
                        .and("pa", -2 * wunden.size)
                        .and("ge", -2 * wunden.size)
                        .and("ini", -2 * wunden.size)
                        .and("gs", -1 * wunden.size)
                WundenLocation.bein_rechts -> if (wunden.size >= 3)
                    State("3 Wunden am rechten Bein")
                        .and("kampfunfähig")
                else
                    State("${wunden.size} Wunde(n) am rechten Bein")
                        .and("at", -2 * wunden.size)
                        .and("pa", -2 * wunden.size)
                        .and("ge", -2 * wunden.size)
                        .and("ini", -2 * wunden.size)
                        .and("gs", -1 * wunden.size)
            }
        }
