package de.grannath.pardona

import de.grannath.pardona.characters.getCharacterStatus
import de.grannath.pardona.characters.getEigenschaften
import de.grannath.pardona.characters.getTalente
import de.grannath.pardona.characters.getZauber
import de.grannath.pardona.characters.parseDocument
import de.grannath.pardona.characters.replaceEigenschaften
import de.grannath.pardona.characters.replaceTalente
import de.grannath.pardona.characters.replaceZauber
import de.grannath.pardona.characters.serialize
import de.grannath.pardona.characters.transaction
import de.grannath.pardona.characters.updateCharacter
import de.grannath.pardona.characters.updateStatusFromDocument
import de.grannath.pardona.jooq.tables.references.CHARACTER
import de.grannath.pardona.jooq.tables.references.INVENTORY
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import mu.KotlinLogging
import org.flywaydb.core.Flyway
import org.flywaydb.core.api.callback.Callback
import org.flywaydb.core.api.callback.Context
import org.flywaydb.core.api.callback.Event
import org.jooq.DSLContext
import org.jooq.impl.DSL
import org.springframework.beans.factory.ObjectProvider
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class FlywayConfig {
    @Bean
    fun flywayInitializer(flyway: Flyway, migrationStrategy: ObjectProvider<FlywayMigrationStrategy>, callback: DatabaseCallback): FlywayMigrationInitializer {
        val withCallbacks = Flyway.configure().configuration(flyway.configuration).callbacks(callback).load()
        return FlywayMigrationInitializer(withCallbacks, migrationStrategy.getIfAvailable())
    }

    @Bean
    fun databaseCallback(): DatabaseCallback = DatabaseCallback()
}

class DatabaseCallback : Callback {
    private val logger = KotlinLogging.logger { }
    private var doMigration = false

    override fun supports(event: Event?, context: Context?): Boolean =
        event in listOf(Event.AFTER_MIGRATE, Event.AFTER_EACH_MIGRATE)

    override fun canHandleInTransaction(event: Event?, context: Context?): Boolean =
        true

    override fun handle(event: Event, context: Context) = runBlocking {
        if (event == Event.AFTER_EACH_MIGRATE) {
            doMigration = true
        } else if (event == Event.AFTER_MIGRATE && doMigration) {
            logger.info { "Migrating characters to the new model." }

            val create = DSL.using(context.connection)
            val allChars = create.select(CHARACTER.ID, CHARACTER.DOCUMENT)
                .from(CHARACTER)
                .fetch()
                .map { it.get(CHARACTER.ID)!! to it.get(CHARACTER.DOCUMENT)!! }

            allChars.forEach { (id, doc) ->
                create.updateCharacterFromDocument(id, doc)
            }

            logger.info { "Migrated ${allChars.size} characters to the new model." }
        }
    }

    override fun getCallbackName(): String =
        "Reparse_all_characters"
}

private suspend fun DSLContext.updateCharacterFromDocument(characterId: Long, document: String): Long = withContext(Dispatchers.IO) {
    val doc = parseDocument(document)
    transaction { dsl: DSLContext ->
        dsl.updateCharacter(characterId, doc.serialize())

        dsl.replaceEigenschaften(characterId, doc.getEigenschaften())
        dsl.replaceTalente(characterId, doc.getTalente())
        dsl.replaceZauber(characterId, doc.getZauber())
        dsl.updateStatusFromDocument(characterId, doc.getCharacterStatus())
        withContext(Dispatchers.IO) {
            dsl.insertInto(INVENTORY, INVENTORY.CHARACTER_ID)
                .values(characterId)
                .onConflictDoNothing()
                .execute()
        }

        characterId
    }
}
