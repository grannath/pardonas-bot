package de.grannath.pardona.commands.status

import de.grannath.pardona.checks.rollExpression
import de.grannath.pardona.commands.Invocation
import de.grannath.pardona.commands.UserException
import de.grannath.pardona.jooq.tables.references.CHARACTER_STATUS
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jooq.DSLContext
import kotlin.math.absoluteValue

suspend fun updateLep(
    invocation: Invocation,
    expression: String,
    subtract: Boolean,
    create: DSLContext,
): String {
    val diff = rollExpression(expression) * if (subtract) -1 else 1

    val status = currentStatus(invocation, create)
    val lepStatus = lepStatus(status.currentLep!!)
    val newCurrent = (status.currentLep + diff).coerceAtMost(status.maxLep!!)
    val newLepStatus = lepStatus(newCurrent)
    val trueDiff = newCurrent - status.currentLep

    create.setCurrentLep(status.id!!, newCurrent)

    return when (lepStatus) {
        LepStatus.ALIVE -> when (newLepStatus) {
            LepStatus.ALIVE -> "${invocation.user.mention}, Ihr habt ${trueDiff.absoluteValue} Lebenspunkte ${if (trueDiff < 0) "verloren" else "erhalten"}. Ihr habt jetzt $newCurrent LeP."
            LepStatus.KO -> "${invocation.user.mention}, Ihr habt ${trueDiff.absoluteValue} Lebenspunkte verloren und seid nun kampfunfähig. Ihr habt noch $newCurrent LeP."
            LepStatus.DEAD -> "${invocation.user.mention}, Ihr habt ${trueDiff.absoluteValue} Lebenspunkte verloren und seid nun so gut wie tot. Ich wünsche euch eine angenehme Reise in die Niederhöllen"
        }
        LepStatus.KO -> when (newLepStatus) {
            LepStatus.ALIVE -> "${invocation.user.mention}, Ihr habt ${trueDiff.absoluteValue} Lebenspunkte erhalten und seid nun wieder bei vollem Bewusstsein. Ihr habt jetzt $newCurrent LeP."
            LepStatus.KO -> "${invocation.user.mention}, Ihr habt ${trueDiff.absoluteValue} Lebenspunkte ${if (trueDiff < 0) "verloren" else "erhalten"}, doch ihr seid trotzdem kampfunfähig. Ihr habt jetzt $newCurrent LeP."
            LepStatus.DEAD -> "${invocation.user.mention}, Ihr habt ${trueDiff.absoluteValue} Lebenspunkte verloren und seid nun so gut wie tot. Ich wünsche euch eine angenehme Reise in die Niederhöllen"
        }
        LepStatus.DEAD -> when (newLepStatus) {
            LepStatus.ALIVE -> "${invocation.user.mention}, Ihr habt ${trueDiff.absoluteValue} Lebenspunkte erhalten. Ihr seid dem Tod noch einmal entkommen. Ihr habt jetzt $newCurrent LeP."
            LepStatus.KO -> "${invocation.user.mention}, Ihr habt ${trueDiff.absoluteValue} Lebenspunkte erhalten. Ihr werdet zwar nicht sterben, aber ihr seid noch kampfunfähig. Ihr habt jetzt $newCurrent LeP."
            LepStatus.DEAD -> if (trueDiff < 0) {
                "${invocation.user.mention}, Ihr habt ${trueDiff.absoluteValue} weitere Lebenspunkte verloren und rückt der Seelenmühle ein Stück näher."
            } else {
                "${invocation.user.mention}, Ihr habt ${trueDiff.absoluteValue} Lebenspunkte erhalten, aber ihr werdet dennoch sterben. Was für ein Unglück."
            }
        }
    }
}

private enum class LepStatus {
    ALIVE, KO, DEAD
}

private fun lepStatus(lep: Int) =
    when {
        lep > 5 -> LepStatus.ALIVE
        lep > 0 -> LepStatus.KO
        else -> LepStatus.DEAD
    }

suspend fun updateAup(
    invocation: Invocation,
    expression: String,
    subtract: Boolean,
    create: DSLContext,
): String {
    val diff = rollExpression(expression) * if (subtract) -1 else 1

    val status = currentStatus(invocation, create)
    val newCurrent = (status.currentAup!! + diff).coerceAtLeast(0).coerceAtMost(status.maxAup!!)
    val trueDiff = newCurrent - status.currentAup

    create.setCurrentAup(status.id!!, newCurrent)
    return if (newCurrent > 0)
        "Ihr habt ${trueDiff.absoluteValue} Ausdauerpunkte ${if (trueDiff < 0) "verloren" else "erhalten"}. Ihr habt ${if (trueDiff < 0) "noch" else "jetzt"} $newCurrent AuP."
    else
        "Ihr habt ${trueDiff.absoluteValue} Ausdauerpunkte verloren und seid nun kampfunfähig. Ihr solltet besser ein wenig rasten."
}

suspend fun updateAsp(
    invocation: Invocation,
    expression: String,
    create: DSLContext,
): String {
    val diff = rollExpression(expression)

    val status = currentStatus(invocation, create)
    if (status.maxAsp == null) {
        throw UserException("Versucht nicht, mich hereinzulegen! Ihr seid gar kein Magier.")
    }

    val newCurrent = (status.currentAsp!! + diff).coerceAtMost(status.maxAsp)
    return if (newCurrent < 0) {
        "Ich lobe euren Ehrgeiz, aber ihr habt nur ${status.currentAsp} AsP, nicht ${diff.absoluteValue}. Sterbliche sollten ihre Grenzen kennen."
    } else {
        val trueDiff = newCurrent - status.currentAsp

        create.setCurrentAsp(status.id!!, newCurrent)
        "Ihr habt ${trueDiff.absoluteValue} Punkte Astralenergie ${if (trueDiff < 0) "verloren" else "erhalten"}. Ihr habt ${if (trueDiff < 0) "noch" else "jetzt"} $newCurrent AsP."
    }
}

suspend fun updateKap(
    invocation: Invocation,
    expression: String,
    create: DSLContext,
): String {
    val diff = rollExpression(expression)

    val status = currentStatus(invocation, create)
    if (status.maxKap == null) {
        throw UserException("Versucht nicht, mich hereinzulegen! Ihr seid gar kein Götterdiener.")
    }

    val newCurrent = (status.currentKap!! + diff).coerceAtMost(status.maxKap)
    return if (newCurrent < 0) {
        "So sehr liebt euch euer Gott wohl nicht. Ihr habt lediglich ${status.currentKap} KaP, und keine ${diff.absoluteValue}."
    } else {
        val trueDiff = newCurrent - status.currentKap

        create.setCurrentKap(status.id!!, newCurrent)
        "Ihr habt ${trueDiff.absoluteValue} Punkte Karmalenergie ${if (trueDiff < 0) "verloren" else "erhalten"}. Ihr habt ${if (trueDiff < 0) "noch" else "jetzt"} $newCurrent KaP."
    }
}

private suspend fun DSLContext.setCurrentLep(characterStatusId: Long, currentLep: Int): Int = withContext(Dispatchers.IO) {
    update(CHARACTER_STATUS)
        .set(CHARACTER_STATUS.CURRENT_LEP, currentLep)
        .where(CHARACTER_STATUS.ID.eq(characterStatusId))
        .returning(CHARACTER_STATUS.CURRENT_LEP)
        .fetchOne()
        ?.get(CHARACTER_STATUS.CURRENT_LEP)!!
}

private suspend fun DSLContext.setCurrentAup(characterStatusId: Long, currentAup: Int): Int = withContext(Dispatchers.IO) {
    update(CHARACTER_STATUS)
        .set(CHARACTER_STATUS.CURRENT_AUP, currentAup)
        .where(CHARACTER_STATUS.ID.eq(characterStatusId))
        .returning(CHARACTER_STATUS.CURRENT_AUP)
        .fetchOne()
        ?.get(CHARACTER_STATUS.CURRENT_AUP)!!
}

private suspend fun DSLContext.setCurrentAsp(characterStatusId: Long, currentAsp: Int): Int = withContext(Dispatchers.IO) {
    update(CHARACTER_STATUS)
        .set(CHARACTER_STATUS.CURRENT_ASP, currentAsp)
        .where(CHARACTER_STATUS.ID.eq(characterStatusId))
        .returning(CHARACTER_STATUS.CURRENT_ASP)
        .fetchOne()
        ?.get(CHARACTER_STATUS.CURRENT_ASP)!!
}

private suspend fun DSLContext.setCurrentKap(characterStatusId: Long, currentKap: Int): Int = withContext(Dispatchers.IO) {
    update(CHARACTER_STATUS)
        .set(CHARACTER_STATUS.CURRENT_KAP, currentKap)
        .where(CHARACTER_STATUS.ID.eq(characterStatusId))
        .returning(CHARACTER_STATUS.CURRENT_KAP)
        .fetchOne()
        ?.get(CHARACTER_STATUS.CURRENT_KAP)!!
}
