package de.grannath.pardona.commands

import de.grannath.pardona.characters.CharacterEigenschaft
import de.grannath.pardona.characters.EigenschaftModifier
import de.grannath.pardona.characters.asString
import de.grannath.pardona.checks.EigenschaftCheckResult
import de.grannath.pardona.checks.EigenschaftCheckResult.CriticalChanceFailure
import de.grannath.pardona.checks.EigenschaftCheckResult.CriticalChanceSuccess
import de.grannath.pardona.checks.EigenschaftCheckResult.CriticalFailure
import de.grannath.pardona.checks.EigenschaftCheckResult.CriticalSuccess
import de.grannath.pardona.checks.EigenschaftCheckResult.Failure
import de.grannath.pardona.checks.EigenschaftCheckResult.Success
import de.grannath.pardona.checks.EigenschaftInput
import de.grannath.pardona.checks.RollSource
import de.grannath.pardona.checks.check
import de.grannath.pardona.checks.defaultRollSource
import de.grannath.pardona.checks.isSuccess
import de.grannath.pardona.checks.rollExpression
import de.grannath.pardona.commands.status.currentState
import de.grannath.pardona.jooq.enums.WundenLocation
import discord4j.core.GatewayDiscordClient
import discord4j.core.`object`.command.ApplicationCommandOption
import discord4j.core.spec.EmbedCreateFields
import discord4j.core.spec.EmbedCreateSpec
import discord4j.discordjson.json.ApplicationCommandOptionData
import discord4j.discordjson.json.ApplicationCommandRequest
import org.jooq.DSLContext
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class AttackCommands(
    private val create: DSLContext,
    client: GatewayDiscordClient,
) : SlashCommand(client) {
    override val command: ApplicationCommandRequest =
        ApplicationCommandRequest.builder()
            .name("attacke")
            .description("Wirf eine Attacke-Probe!")
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("at")
                    .description("Dein Angriffswert")
                    .type(ApplicationCommandOption.Type.INTEGER.value)
                    .required(true)
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("modifikator")
                    .description("Eine Erschwernis oder Erleichterung")
                    .type(ApplicationCommandOption.Type.INTEGER.value)
                    .required(false)
                    .build()
            )
            .addOption(characterOption)
            .addOption(recipientOption)
            .build()

    override suspend fun handle(event: Invocation) {
        val stateModifier = currentState(event, create)
            .modifiers
            .find { it is EigenschaftModifier && it.name == "at" }
            ?.modifier
            ?: 0
        val at = event.getIntOptionValue("at")!!
        val mod = event.getIntOptionValue("modifikator") ?: 0
        val response = EigenschaftInput.Named(CharacterEigenschaft("Attacke", at), mod, stateModifier).check()
            .toAttackeMessage(event)

        event.reply(response)
    }
}

@Component
class ParadeCommands(
    private val create: DSLContext,
    client: GatewayDiscordClient,
) : SlashCommand(client) {
    override val command: ApplicationCommandRequest =
        ApplicationCommandRequest.builder()
            .name("parade")
            .description("Wirf eine Parade-Probe!")
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("pa")
                    .description("Dein Paradewert")
                    .type(ApplicationCommandOption.Type.INTEGER.value)
                    .required(true)
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("modifikator")
                    .description("Eine Erschwernis oder Erleichterung")
                    .type(ApplicationCommandOption.Type.INTEGER.value)
                    .required(false)
                    .build()
            )
            .addOption(characterOption)
            .addOption(recipientOption)
            .build()

    override suspend fun handle(event: Invocation) {
        val stateModifier = currentState(event, create)
            .modifiers
            .find { it is EigenschaftModifier && it.name == "pa" }
            ?.modifier
            ?: 0
        val pa = event.getIntOptionValue("pa")!!
        val mod = event.getIntOptionValue("modifikator") ?: 0
        val response = EigenschaftInput.Named(CharacterEigenschaft("Parade", pa), mod, stateModifier).check()
            .toParadeMessage(event)

        event.reply(response)
    }
}

private fun EigenschaftCheckResult.toAttackeMessage(invocation: Invocation): EmbedCreateSpec =
    EmbedCreateSpec.create()
        .withTitle("Attackeprobe")
        .withDescription("${invocation.user.mention}, ${personalMessage()}")
        .withTimestamp(Instant.now())
        .withColor(color())
        .withFields(detailsFields(this, trefferzone()))
        .withFooter(EmbedCreateFields.Footer.of("Pardona's Bot", null))

private fun EigenschaftCheckResult.toParadeMessage(invocation: Invocation): EmbedCreateSpec =
    EmbedCreateSpec.create()
        .withTitle("Paradeprobe")
        .withDescription("${invocation.user.mention}, ${personalMessage()}")
        .withTimestamp(Instant.now())
        .withColor(color())
        .withFields(detailsFields(this, null))
        .withFooter(EmbedCreateFields.Footer.of("Pardona's Bot", null))

private fun EigenschaftCheckResult.color() =
    when (this) {
        is CriticalChanceFailure -> COLOR_FAILURE
        is CriticalChanceSuccess -> COLOR_SUCCESS
        is CriticalFailure -> COLOR_CRIT_FAILURE
        is CriticalSuccess -> COLOR_CRIT_SUCCESS
        is Failure -> COLOR_FAILURE
        is Success -> COLOR_SUCCESS
    }

private fun detailsFields(result: EigenschaftCheckResult, zone: WundenLocation?): List<EmbedCreateFields.Field> =
    listOf(
        eigenschaftField(result.input),
        modifierField(result.input)
    ) + resultFields(result, zone)

private fun eigenschaftField(input: EigenschaftInput): EmbedCreateFields.Field =
    when (input) {
        is EigenschaftInput.Number -> {
            EmbedCreateFields.Field.of(
                "Eigenschaft",
                "${input.rawValue}",
                true,
            )
        }
        is EigenschaftInput.Named -> {
            EmbedCreateFields.Field.of(
                if (input.stateModifier != 0) "${input.eigenschaft.shortForm}${input.stateModifier.signedString()}" else input.eigenschaft.shortForm,
                (input.eigenschaft.value + input.stateModifier).toString(),
                true,
            )
        }
    }

private fun modifierField(input: EigenschaftInput): EmbedCreateFields.Field =
    when (input) {
        is EigenschaftInput.Named ->
            EmbedCreateFields.Field.of(
                "Modifikator",
                input.userModifier.toString(),
                true,
            )
        is EigenschaftInput.Number ->
            EmbedCreateFields.Field.of(
                "Modifikator",
                input.modifier.toString(),
                true,
            )
    }

private fun resultFields(result: EigenschaftCheckResult, zone: WundenLocation?): List<EmbedCreateFields.Field> =
    when (result) {
        is CriticalFailure ->
            listOf(
                EmbedCreateFields.Field.of("Wurf", "20", true),
                EmbedCreateFields.Field.of("Bestätigung", result.secondRoll.toString(), true),
                EmbedCreateFields.Field.of("max. Erschwernis", result.maxModifier?.toString() ?: "-", true),
                EmbedCreateFields.Field.of("Patzerfolgen", patzer(), false),
            )
        is CriticalChanceFailure ->
            listOf(
                EmbedCreateFields.Field.of("Wurf", "20", true),
                EmbedCreateFields.Field.of("Bestätigung", result.secondRoll.toString(), true),
                EmbedCreateFields.Field.of("max. zusätzl. Erschwernis", result.maxAddModifier?.toString() ?: "-", true),
            )
        is CriticalSuccess ->
            listOfNotNull(
                EmbedCreateFields.Field.of("Wurf", "1", true),
                EmbedCreateFields.Field.of("Bestätigung", result.secondRoll.toString(), true),
                EmbedCreateFields.Field.of("max. zusätzl. Erschwernis", result.maxAddModifier?.toString() ?: "-", true),
                zone?.let { EmbedCreateFields.Field.of("Trefferzone", it.asString(), false) },
            )
        is CriticalChanceSuccess ->
            listOfNotNull(
                EmbedCreateFields.Field.of("Wurf", "1", true),
                EmbedCreateFields.Field.of("Bestätigung", result.secondRoll.toString(), true),
                EmbedCreateFields.Field.of("max. Erschwernis", result.maxModifier?.toString() ?: "-", true),
                zone?.let { EmbedCreateFields.Field.of("Trefferzone", it.asString(), false) },
            )
        is Failure ->
            listOf(
                EmbedCreateFields.Field.of("Wurf", result.roll.toString(), true),
                EmbedCreateFields.Field.of("max. Erschwernis", result.maxModifier.toString(), true),
            )
        is Success ->
            listOfNotNull(
                EmbedCreateFields.Field.of("Wurf", result.roll.toString(), true),
                EmbedCreateFields.Field.of("max. zusätzl. Erschwernis", result.maxAddModifier.toString(), true),
                zone?.let { EmbedCreateFields.Field.of("Trefferzone", it.asString(), false) },
            )
    }

private fun EigenschaftCheckResult.personalMessage() =
    when (this) {
        is CriticalFailure ->
            "Was für ein schreckliches Schicksal, Sterbliche(r)!"
        is CriticalChanceFailure ->
            "Da seid Ihr ja nur knapp einer Katastrophe entgangen."
        is CriticalSuccess ->
            "Unglaublich! Das ein(e) Sterbliche(r) zu solchen Leistungen im Stande ist."
        is CriticalChanceSuccess ->
            "Das war nicht schlecht, aber Ihr bleibt halt sterblich."
        else -> null
    }

private fun EigenschaftCheckResult.trefferzone(rollSource: RollSource = defaultRollSource) =
    if (this.isSuccess())
        when (rollSource(20)) {
            in listOf(1, 3, 5) -> WundenLocation.bein_links
            in listOf(2, 4, 6) -> WundenLocation.bein_rechts
            in listOf(7, 8) -> WundenLocation.bauch
            in listOf(9, 11, 13) -> WundenLocation.arm_links
            in listOf(10, 12, 14) -> WundenLocation.arm_rechts
            in listOf(15, 16, 17, 18) -> WundenLocation.brust
            in listOf(19, 20) -> WundenLocation.kopf
            else -> throw RuntimeException("Too high number for hit zones!")
        }
    else
        null

private fun patzer(rollSource: RollSource = defaultRollSource) =
    when (rollExpression("2W6", rollSource)) {
        2 -> "Eure Waffe ist zerstört."
        in 3..5 -> "Ihr liegt nun am Boden."
        in 6..8 -> "Ihr seid gestolpert."
        in 9..10 -> "Ihr habt eure Waffe verloren."
        11 -> "Ihr habt euch selbst verletzt."
        12 -> "Ihr habt euch sehr schwer verletzt."
        else -> throw RuntimeException("Too high number for Patzer.")
    }
