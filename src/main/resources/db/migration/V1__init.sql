CREATE TABLE "user"
(
    "id"                    bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    "discord_id"            varchar NOT NULL,
    "selected_character_id" bigint
);

CREATE TABLE "character"
(
    "id"       bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    "user_id"  bigint  NOT NULL,
    "name"     varchar NOT NULL,
    "document" varchar NOT NULL
);

ALTER TABLE "user"
    ADD CONSTRAINT "FK_user_selected_character"
        FOREIGN KEY ("selected_character_id") REFERENCES "character" ("id");

ALTER TABLE "character"
    ADD CONSTRAINT "FK_character_user"
        FOREIGN KEY ("user_id") REFERENCES "user" ("id");

ALTER TABLE "character"
    ADD CONSTRAINT "UNQ_character_user_name"
        UNIQUE ("user_id", "name");

CREATE TABLE "eigenschaft_value"
(
    "id"           bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    "name"         varchar NOT NULL,
    "value"        int     NOT NULL,
    "character_id" bigint  NOT NULL
);

ALTER TABLE "eigenschaft_value"
    ADD CONSTRAINT "FK_eigenschaft_character"
        FOREIGN KEY ("character_id") REFERENCES "character" ("id");

CREATE TABLE "talent_value"
(
    "id"           bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    "name"         varchar NOT NULL,
    "value"        int     NOT NULL,
    "check_string" varchar NOT NULL,
    "character_id" bigint  NOT NULL
);

ALTER TABLE "talent_value"
    ADD CONSTRAINT "FK_talent_character"
        FOREIGN KEY ("character_id") REFERENCES "character" ("id");

CREATE TABLE "zauber_value"
(
    "id"           bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    "name"         varchar NOT NULL,
    "value"        int     NOT NULL,
    "check_string" varchar NOT NULL,
    "character_id" bigint  NOT NULL
);

ALTER TABLE "zauber_value"
    ADD CONSTRAINT "FK_zauber_character"
        FOREIGN KEY ("character_id") REFERENCES "character" ("id");
