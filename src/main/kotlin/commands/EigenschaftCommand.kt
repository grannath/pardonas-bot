package de.grannath.pardona.commands

import de.grannath.pardona.characters.AllEigenschaftenModifier
import de.grannath.pardona.characters.EigenschaftModifier
import de.grannath.pardona.characters.fetchEigenschaften
import de.grannath.pardona.checks.EigenschaftCheckResult
import de.grannath.pardona.checks.EigenschaftCheckResult.CriticalChanceFailure
import de.grannath.pardona.checks.EigenschaftCheckResult.CriticalChanceSuccess
import de.grannath.pardona.checks.EigenschaftCheckResult.CriticalFailure
import de.grannath.pardona.checks.EigenschaftCheckResult.CriticalSuccess
import de.grannath.pardona.checks.EigenschaftCheckResult.Failure
import de.grannath.pardona.checks.EigenschaftCheckResult.Success
import de.grannath.pardona.checks.EigenschaftInput
import de.grannath.pardona.checks.check
import de.grannath.pardona.commands.status.currentState
import discord4j.core.GatewayDiscordClient
import discord4j.core.`object`.command.ApplicationCommandOption
import discord4j.core.spec.EmbedCreateFields
import discord4j.core.spec.EmbedCreateSpec
import discord4j.discordjson.json.ApplicationCommandOptionData
import discord4j.discordjson.json.ApplicationCommandRequest
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import org.jooq.DSLContext
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class EigenschaftCommand(
    private val create: DSLContext,
    client: GatewayDiscordClient,
) : SlashCommand(client) {
    override val command: ApplicationCommandRequest =
        ApplicationCommandRequest.builder()
            .name("eigenschaft")
            .description("Wirf eine Eigenschaftsprobe")
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("eigenschaft")
                    .description("Dein Eigenschaftswert, als Zahl oder Eigenschaftsname")
                    .type(ApplicationCommandOption.Type.STRING.value)
                    .required(true)
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("modifikator")
                    .description("Eine Erleichterung oder Erschwernis")
                    .type(ApplicationCommandOption.Type.INTEGER.value)
                    .required(false)
                    .build()
            )
            .addOption(characterOption)
            .addOption(recipientOption)
            .build()

    override suspend fun handle(event: Invocation) {
        val response = coroutineScope {
            val input = event.getStringOptionValue("eigenschaft")!!

            val eigenschaftInput = create.fetchEigenschaftInput(event, input, userModifier = true)[0]
            eigenschaftInput.check().toMessage(event)
        }

        event.reply(response)
    }
}

private fun EigenschaftCheckResult.toMessage(invocation: Invocation): EmbedCreateSpec =
    EmbedCreateSpec.create()
        .withTitle("Eigenschaftsprobe")
        .withDescription("${invocation.user.mention}, ${personalMessage()}")
        .withTimestamp(Instant.now())
        .withColor(color())
        .withFields(detailsFields(this))
        .withFooter(EmbedCreateFields.Footer.of("Pardona's Bot", null))

suspend fun DSLContext.fetchEigenschaftInput(
    invocation: Invocation,
    vararg inputs: String,
    userModifier: Boolean,
): List<EigenschaftInput> = coroutineScope {
    val eigenschaften = async {
        this@fetchEigenschaftInput.fetchEigenschaften(invocation.user.id, invocation.characterName)
    }

    val state = async { currentState(invocation, this@fetchEigenschaftInput) }

    inputs.map { input ->
        if (input.toIntOrNull() == null) {
            val candidates =
                eigenschaften.await()
                    .filter { it.name.startsWith(input, ignoreCase = true) || it.shortForm.startsWith(input, ignoreCase = true) }
            val characterEigenschaft = when {
                eigenschaften.await().isEmpty() -> throw UserException("Gebt mir bitte eine Zahl als Eigenschaftswert.")
                candidates.isEmpty() -> throw UserException("Ihr habt keine Eigenschaft $input.")
                candidates.size > 1 -> throw UserException("Welche eurer Eigenschaften meint ihr? ${candidates.joinToString(separator = ", ", limit = 3) { it.name }}")
                else -> candidates[0]
            }
            val stateModifier = state.await()
                .modifiers
                .filter { it is AllEigenschaftenModifier || it is EigenschaftModifier && it.name == characterEigenschaft.shortForm }
                .sumOf { it.modifier }
            EigenschaftInput.Named(
                eigenschaft = characterEigenschaft,
                userModifier = if (userModifier) invocation.getIntOptionValue("modifikator") ?: 0 else 0,
                stateModifier = stateModifier,
            )
        } else {
            EigenschaftInput.Number(
                rawValue = input.toInt(),
                modifier = if (userModifier) invocation.getIntOptionValue("modifikator") ?: 0 else 0,
            )
        }
    }
}

private fun EigenschaftCheckResult.color() =
    when (this) {
        is CriticalChanceFailure -> COLOR_FAILURE
        is CriticalChanceSuccess -> COLOR_SUCCESS
        is CriticalFailure -> COLOR_CRIT_FAILURE
        is CriticalSuccess -> COLOR_CRIT_SUCCESS
        is Failure -> COLOR_FAILURE
        is Success -> COLOR_SUCCESS
    }

private fun EigenschaftCheckResult.personalMessage() =
    when (this) {
        is CriticalFailure ->
            "Was für ein schreckliches Schicksal, Sterbliche(r)!"
        is CriticalChanceFailure ->
            "Da seid Ihr ja nur knapp einer Katastrophe entgangen."
        is CriticalSuccess ->
            "Unglaublich! Das ein(e) Sterbliche(r) zu solchen Leistungen im Stande ist."
        is CriticalChanceSuccess ->
            "Das war nicht schlecht, aber Ihr bleibt halt sterblich."
        is Failure ->
            "Das war wohl nichts. Viel Glück beim nächsten Mal."
        is Success ->
            "Ein Erfolg, für eine(n) Sterbliche(n)."
    }

private fun detailsFields(result: EigenschaftCheckResult): List<EmbedCreateFields.Field> =
    listOf(
        eigenschaftField(result.input),
        modifierField(result.input)
    ) + resultFields(result)

private fun eigenschaftField(input: EigenschaftInput): EmbedCreateFields.Field =
    when (input) {
        is EigenschaftInput.Number -> {
            EmbedCreateFields.Field.of(
                "Eigenschaft",
                "${input.rawValue}",
                true,
            )
        }
        is EigenschaftInput.Named -> {
            EmbedCreateFields.Field.of(
                if (input.stateModifier != 0) "${input.eigenschaft.shortForm}${input.stateModifier.signedString()}" else input.eigenschaft.shortForm,
                (input.eigenschaft.value + input.stateModifier).toString(),
                true,
            )
        }
    }

private fun modifierField(input: EigenschaftInput): EmbedCreateFields.Field =
    when (input) {
        is EigenschaftInput.Named ->
            EmbedCreateFields.Field.of(
                "Modifikator",
                input.userModifier.toString(),
                true,
            )
        is EigenschaftInput.Number ->
            EmbedCreateFields.Field.of(
                "Modifikator",
                input.modifier.toString(),
                true,
            )
    }

private fun resultFields(result: EigenschaftCheckResult): List<EmbedCreateFields.Field> =
    when (result) {
        is CriticalFailure ->
            listOf(
                EmbedCreateFields.Field.of("Wurf", "20", true),
                EmbedCreateFields.Field.of("Bestätigung", result.secondRoll.toString(), true),
                EmbedCreateFields.Field.of("max. Erschwernis", result.maxModifier?.toString() ?: "-", true),
            )
        is CriticalChanceFailure ->
            listOf(
                EmbedCreateFields.Field.of("Wurf", "20", true),
                EmbedCreateFields.Field.of("Bestätigung", result.secondRoll.toString(), true),
                EmbedCreateFields.Field.of("max. zusätzl. Erschwernis", result.maxAddModifier?.toString() ?: "-", true),
            )
        is CriticalSuccess ->
            listOf(
                EmbedCreateFields.Field.of("Wurf", "1", true),
                EmbedCreateFields.Field.of("Bestätigung", result.secondRoll.toString(), true),
                EmbedCreateFields.Field.of("max. zusätzl. Erschwernis", result.maxAddModifier?.toString() ?: "-", true),
            )
        is CriticalChanceSuccess ->
            listOf(
                EmbedCreateFields.Field.of("Wurf", "1", true),
                EmbedCreateFields.Field.of("Bestätigung", result.secondRoll.toString(), true),
                EmbedCreateFields.Field.of("max. Erschwernis", result.maxModifier?.toString() ?: "-", true),
            )
        is Failure ->
            listOf(
                EmbedCreateFields.Field.of("Wurf", result.roll.toString(), true),
                EmbedCreateFields.Field.of("max. Erschwernis", result.maxModifier.toString(), true),
            )
        is Success ->
            listOf(
                EmbedCreateFields.Field.of("Wurf", result.roll.toString(), true),
                EmbedCreateFields.Field.of("max. zusätzl. Erschwernis", result.maxAddModifier.toString(), true),
            )
    }
