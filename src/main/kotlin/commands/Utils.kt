package de.grannath.pardona.commands

import discord4j.rest.util.Color
import java.util.StringJoiner

fun StringJoiner.addIfNotNull(newElement: CharSequence?): StringJoiner =
    if (newElement != null)
        add(newElement)
    else
        this

fun Int.signedString() =
    if (this < 0) {
        toString()
    } else {
        "+$this"
    }

val COLOR_SUCCESS = Color.GREEN
val COLOR_FAILURE = Color.RED
val COLOR_CRIT_SUCCESS = Color.YELLOW
val COLOR_CRIT_FAILURE = Color.JAZZBERRY_JAM
